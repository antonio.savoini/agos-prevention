import { get, post, put, destroy } from './Intercepter';
const ApiFactory = {
    Users: {
        USER_AUTHORIZED: (headers) => get('/api/user/authorized', headers),
        USER_EXISTS: (username, headers) => get(`/api/user/user-exists/${username}`, headers),
        CREATE_USER: (params, headers) => post('/api/user/create-user', params, headers),
        USER_VERIFY: (params) => post('/api/user/verify', params)
    },
    Company: {
        COMPANY_LIST: (headers) => get('/api/company/company-list',headers)
    },
    Pay: {
        PAYMENT_BY_CAMPAIGN: (campaignID,headers) => get(`/api/pay/payment-by-campaign/${campaignID}`,headers),
        PAYMENT_BY_DATE: (companyID,startDate,endDate,headers) => get(`/api/pay/payment-by-date/${companyID}/${startDate}/${endDate}`,headers),
        AXERVE: (params) => post('/api/pay/axerve',params),
        AXERVE_PAYMENT:(params) => post('/api/pay/axerve-payment',params),
        STRIPE:(params) => post('/api/pay/stripe',params),
        STRIPE_PAYMENT:(params) => post('/api/pay/stripe-payment',params),
        GET_AMOUNT: (params) => post('/api/pay/get-amount-multi', params),
        XBUILD_IS_RECUR: (params) => post('/api/pay/xbuild-is-recur',params),
        XBUILD_NONCE:(params) => post('/api/pay/xbuild-nonce',params),
        XBUILD_RECUR:(params) => post('/api/pay/xbuild-recur',params),
        //Form post url method
        XBUILD: '/api/pay/xbuild'
    },
    Campaign: {
        CAMPAIGN_BY_COMPANY: (campaignID, headers) => get(`/api/campaign/get-campaign-by-company/${campaignID}`, headers),
    },
    SMS: {
        STATUS_BY_CAMPAIGN:(campaignID, headers) => get(`/api/sms/status-by-campaign/${campaignID}`, headers),
        STATUS_BY_DATE:(companyID,startDate,endDate,headers) => get(`/api/sms/status-by-date/${companyID}/${startDate}/${endDate}`,headers)
    },
    TinyURL: {
        GET_CONTACT: (params) => post('/api/tinyurl/get-contact',params),
        TINYURL: (params) => post('/api/tinyurl',params)
    },
    Enghouse:{
        GET_IDPRATICA: (params) => get(`/agos/api/personalizzazioni/agos/get-idpratica/${params}`),
        GET_DESCRIPTIONS: (params) => get(`/agos/api/personalizzazioni/agos/get-descriptions/${params}`)
    },
    // If you want to send messages from admin panel
    // then you have to change this endpoint
    NWG: {
        SEND_INVOICE: (params, headers) => post('/api/nwg/send-invoice', params, headers)
    },
    // if you want to upload photos from clients
    // then you need to change this endpoint
    Enegan: {
        PHOTOUPLOAD:(params) => post('/enghouse/api/personalizzazioni/enghouse/photoupload',params)
    },
    Request: {
        GET_REQUEST_DETAILS:(params) => get(`/api/request/get-request-details/${params}`)
    },
    Agos: {
        POST_INCASSO_PAY: (params) => post(`/agos/agos-api/incasso-pay`, params),
        RECEIPT: (params) => post(`/agos/agos-api/genera-ricevuta`, params),
        AZIONE_TO_PAY: (params) => post(`/agos/agos-api/azione-to-pay`, params),
        HAS_AZIONE_REQUEST:(params) => get(`/agos/agos-api/has-set-azione-request/${params}`),
        GET_ENCODE_STRING: (params) => post(`/agos/agos-api/encodestring`, params),
        GET_DECODE_STRING: (params) => post(`/agos/agos-api/decodestring`, params),
        DO_CLICK_TO_QR: (params) => post(`/agos/agos-api/doClickToQR`, params),
        UPDATE_REQUEST: (params) => put(`/api/request/update-request`, params),
        POST_INCASSO_PAY_QR: (params) => post(`/agos/agos-api/incasso-pay-qr`, params),
        WEB_REQUEST: (params) => post(`/agos/agos-api/webrequest`, params)
    },
    Mercury: {
        INIT: (params, headers) => post(`/api/mercury/init`, params, headers)
    }
}
export default ApiFactory;