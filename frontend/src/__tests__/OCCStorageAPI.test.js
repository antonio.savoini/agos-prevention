import OCCStorageAPI from "../OCCStorageAPI";

it("OCCStorageAPI get all", () => {
    expect(OCCStorageAPI.getAll()).toStrictEqual({})
})

it("OCCStorageAPI clear", () => {
    expect(OCCStorageAPI.clear()).toBeUndefined()
})

it("OCCStorageAPI set all", () => {
    expect(OCCStorageAPI.setAll({ dummyProperty: 21 })).toBeUndefined()
})

it("OCCStorageAPI set single", () => {
    expect(OCCStorageAPI.setSingle("dummyProperty", 24)).toBeUndefined()
})


it("OCCStorageAPI get single no value", () => {
    //invalid property
    expect(OCCStorageAPI.getSingle("dummyProperty2")).toBeFalsy()
})

it("OCCStorageAPI get single with value", () => {
    //valid property
    expect(OCCStorageAPI.getSingle("dummyProperty")).toBe(24)
})

it("OCCStorageAPI does exist type", () => {
    expect(typeof OCCStorageAPI.doesExist()).toBe("boolean")
})

it("OCCStorageAPI does exist value", () => {
    expect(OCCStorageAPI.doesExist()).toBe(true)
})