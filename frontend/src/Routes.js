import React from "react";
import { createBrowserHistory } from "history";
import { Router, Switch, Route } from "react-browser-router";

import {
  Login,
  InvoiceSender,
  CreateUser,
  NotFound,
  SMSReport,
  PaymentReport,
  decrypt,
} from "./components";

import { MainWrapper } from "./components/template/SHARED";

import { appConfig } from "./config";
import ApiFactory from "./endpoints";
// import PrivateRoute from './PrivateRoute'

const isAuthticated = () => {
  const token = localStorage.getItem("token");

  if (token === "token_expired") {
    return false;
  } else if (token) {
    return ApiFactory.Users.USER_AUTHORIZED({
      headers: { authorization: token },
    })
      .then(({ data }) => {
        if (data.isAuth) {
          return true;
        }
      })
      .catch((err) => {
        console.log("Error: ", err);
        //const { data } = err.response
        //comment out the line down and uncomment the above line also the line 49
        const data = "token_expired";
        localStorage.setItem("token", data);
        window.location = `${appConfig.PATH}/login`;
        return false;
      });
  }
};

const Routes = () => {
  // const authenticated = isAuthticated()
  const roleStored = localStorage.getItem("role");
  //const role = roleStored !== null ? decrypt(roleStored) : null
  const role = null;
  const history = createBrowserHistory();
  return (
    <Router history={history} basename={appConfig.PATH}>
      <Switch>
        {/* Wait page after Agos pre redirection */}
        <Route
          exact
          path={`${appConfig.PATH}/${appConfig.PATH_PREFIX}-landing/:token`}
          component={MainWrapper}
        />

        <Route
          exact
          path={`${appConfig.PATH}/${appConfig.PATH_PREFIX}-select/:token`}
          component={MainWrapper}
        />

        <Route
          exact
          path={`${appConfig.PATH}/${appConfig.PATH_PREFIX}-response`}
          component={MainWrapper}
        />

        <Route
          exact
          path={`${appConfig.PATH}/${appConfig.QRCodePage}`}
          component={MainWrapper}
        />
        <Route
          exact
          path={`${appConfig.PATH}/${appConfig.PATH_PREFIX}-paywith/:token`}
          component={MainWrapper}
        />

        <Route
          exact
          path={`${appConfig.PATH}/${appConfig.PATH_PREFIX}-pay/:token`}
          component={MainWrapper}
        />

        <Route
          exact
          path={`${appConfig.PATH}/${appConfig.PATH_PREFIX}-confirm/:token`}
          component={MainWrapper}
        />

        <Route
          exact
          path={`${appConfig.PATH}/${appConfig.PATH_PREFIX}-camera/:token`}
          component={MainWrapper}
        />

        <Route
          exact
          path={`${appConfig.PATH}/${appConfig.PATH_PREFIX}-stripe/:token`}
          component={MainWrapper}
        />

        <Route
          exact
          path={`${appConfig.PATH}/${appConfig.PATH_PREFIX}-axerve/:token`}
          component={MainWrapper}
        />

        <Route
          exact
          path={`${appConfig.PATH}/return`}
          component={MainWrapper}
        />

        <Route
          exact
          path={`${appConfig.PATH}/agoswebpay`}
          component={MainWrapper}
        />

        <Route component={NotFound} />

        {/* <Route
          exact
          path={`${appConfig.PATH}/`}
          render={props => <Login {...props} authenticated={authenticated} />}
        />
        <Route
          exact
          path={`${appConfig.PATH}/login`}
          render={props => <Login {...props} authenticated={authenticated} />}
        />
        <PrivateRoute
          authenticated={authenticated}
          exact
          path={`${appConfig.PATH}/dashboard`}
          component={InvoiceSender}
        />
        {role === '2' ? (
          <>
            <PrivateRoute
              authenticated={authenticated}
              exact
              path={`${appConfig.PATH}/sms-report`}
              component={SMSReport}
            />
            <PrivateRoute
              authenticated={authenticated}
              exact
              path={`${appConfig.PATH}/payment-report`}
              component={PaymentReport}
            />
            <PrivateRoute
              authenticated={authenticated}
              exact
              path={`${appConfig.PATH}/create-user`}
              component={CreateUser}
            />
          </>
        ) : null} */}
      </Switch>
    </Router>
  );
};

export default Routes;
