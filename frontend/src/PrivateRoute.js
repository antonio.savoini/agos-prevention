import React from 'react';
import { Redirect, Route } from 'react-browser-router';
import Main from './components/admin-panel/Main';
import {appConfig} from './config'
const PrivateRoute  = ({component: Component,authenticated,  ...rest}) => {
  
    return (
      <Route
        {...rest}
        render={(props) => authenticated
          ? <Main><Component {...props} /></Main>
          : <Redirect to={`${appConfig.PATH}/login`}/>}
      />
    )
  }

  export default PrivateRoute;