/**
 * Local storage for the OCC41 response persistency
 */
import { isEmpty } from "lodash"

class OCCStorageAPI {
    #_key = "ctxid";

    getAll() {
        let storageResponse = localStorage.getItem(this.#_key)
        return storageResponse ? JSON.parse(storageResponse) : {}
    }

    clear() {
       localStorage.removeItem(this.#_key)
    }

    setAll(object = {}) {
        localStorage.setItem(this.#_key, JSON.stringify(object))
    }

    //This method is used for both setting and updating
    setSingle(key, value) {
        let response = this.getAll()
        if(response) {
            response = {...response, [key]: value}
        }

        this.setAll(response)
    }

    getSingle(key) {
        let parsed = this.getAll(this.#_key)
        return parsed ? parsed[key] : ''
    }

    doesExist() {
        return !(isEmpty(this.getAll()))
    }
}

export default new OCCStorageAPI()