import React, { useEffect, useState } from 'react';
import _ from 'lodash';
import Alert from '../helpers/Alert';
import Grid from '../admin-panel/Grid'
import DatePicker from 'react-datepicker';
import {appConfig} from '../../config'
import ApiFactory from '../../endpoints'
const SMSReport = () => {

    const [formValues, setFormValues] = useState({});
    //sets company list options htmlFor select
    const [campaignOptions, setCampaignOptions] = useState([]);
    const [filterBy, setFilterBy] = useState(true);
    const [gridData, setGridData] = useState([]);
    const [gridApi, setGridApi] = useState();

    useEffect(() => {
        getCampaigns();
    }, [])

    useEffect(() => {

        const companyID = appConfig.COMPANYID;
        const campaignID = formValues['campaign'];
        const startDate = formValues['startDate'];
        const endDate = formValues['endDate'];

        if (companyID) {
            getCampaigns();
        }
        if (companyID && campaignID && filterBy) {
            getSMSStatusByCampaign(campaignID)
        }
        else if (companyID && startDate && endDate && !filterBy) {
            getSMSStatusByDate(companyID, startDate, endDate);
        }
        // eslint-disable-next-line  
    }, [formValues])

    const onGridReady = params => {
        setGridApi(params.api);
        //gridColumnApi = params.columnApi;
    };



    const onBtnExport = () => {
        gridApi.exportDataAsCsv();
    }

    const columnDefs = [
        { headerName: "SMS ID", field: "sms_id" },
        { headerName: "Mobile Number", field: "mobile_number" },
        { headerName: "Status", field: "status" },
        { headerName: "Order ID", field: "order_id" },
        { headerName: "Created At", field: "created_at" },
        { headerName: "Updated At", field: "updated_at" },

    ]


    const handleControl = (control, value) => {
        setFormValues({ ...formValues, [control]: value });
    }


    const getSMSStatusByCampaign = async (campaignID) => {
        const token = localStorage.getItem('token');
        const { data } = await ApiFactory.SMS.STATUS_BY_CAMPAIGN(campaignID, { headers: { authorization: token } });
        setGridData(data);
    }
    const getSMSStatusByDate = async (companyID, startDate, endDate) => {
        const token = localStorage.getItem('token');
        const { data } = await ApiFactory.SMS.STATUS_BY_DATE(companyID,startDate,endDate, { headers: { authorization: token } });
        setGridData(data);
    }

    const getCampaigns = async () => {
        try {
            const token = localStorage.getItem('token');
            if (appConfig.COMPANYID) {
                const campaigns = await ApiFactory.Campaign.CAMPAIGN_BY_COMPANY(appConfig.COMPANYID, { headers: { authorization: token } });
                // eslint-disable-next-line array-callback-return
                if (_.isObject(campaigns)) {
                    // eslint-disable-next-line array-callback-return
                    setCampaignOptions([]);
                    // eslint-disable-next-line
                    campaigns.data.map(({ campaign_id, campaign_name }) => {
                        setCampaignOptions(options => [...options, { label: campaign_name, value: campaign_id }]);
                    });
                } else if (campaigns === 'token_expired') {
                    localStorage.setItem('token', 'token_expired');
                }
            }

        } catch (err) {
            console.log("TCL: getCampaigns -> err", err.response)
            const { data } = err.response;
            Alert('error', 'Server Error', data, 'fa-exclamation');
        }
    }

   


    return (
        <>
            <style>{`.ag-root-wrapper{width:100%} .react-datepicker{width:auto}`}</style>
            <div className="col-lg-8">
                <h1>SMS Report</h1>
                <hr/>
                <div className="form-group">
                    <div className="col-lg">
                        <label className="mr-3">Filter By</label>
                        <div className="form-check form-check-inline d-flex">

                            <div className="custom-control custom-radio mr-3">
                                <input id="campaign" type="radio" className="custom-control-input" onChange={() => setFilterBy(true)} checked={filterBy} />
                                <label className="custom-control-label form-control" htmlFor="campaign">Campaign</label>
                            </div>
                            <div className="custom-control custom-radio mr-3">
                                <input id="daterange" type="radio" className="custom-control-input" onChange={() => setFilterBy(false)} checked={!filterBy} />
                                <label className="custom-control-label form-control" htmlFor="daterange">Date Range</label>
                            </div>
                        </div>
                    </div>
                    {filterBy && <div className="col-sm-9 mt-3">
                        <label className="mr-3">Campaign List</label>
                        <select
                            className="form-control"
                            onChange={e => handleControl('campaign', e.target.value)}
                            value={formValues['campaign']}
                        >
                            <option value="">Select campaign</option>
                            {
                                campaignOptions && campaignOptions.map(({ label, value }, index) => {
                                    return <option key={index} value={value}>{label}</option>
                                })
                            }
                        </select>
                    </div>
                    }
                    {!filterBy && <div className="col-sm-9 mt-3">
                        <label className="mr-3">Start Date</label>
                        <DatePicker
                            selected={formValues['startDate']}
                            onChange={date => handleControl('startDate', date)}
                            selectsStart
                            startDate={formValues['startDate']}
                            endDate={formValues['endDate']}
                            className="form-control"
                        />
                        <label className="mr-3">End Date</label>
                        <DatePicker
                            selected={formValues['endDate']}
                            onChange={date => handleControl('endDate', date)}
                            selectsEnd
                            startDate={formValues['startDate']}
                            endDate={formValues['endDate']}
                            minDate={formValues['startDate']}
                            className="form-control"
                        />
                    </div>
                    }
                    <div className="mt-3">
                        <label className="mr-3">SMS Status</label>
                        <button className="btn btn-primary float-right" onClick={() => onBtnExport()}>Export to CSV</button>
                        <Grid
                            columnDefs={columnDefs}
                            rowData={gridData}
                            onGridReady={onGridReady}
                        />
                    </div>
                </div>
            </div>

        </>
    )
}

export default SMSReport
