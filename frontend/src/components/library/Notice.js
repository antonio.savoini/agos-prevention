import React from 'react'

import style from '../../theme/Style.module.css'

//${style.box} ${style.orangecolor} ${style.whitecolor}
//
export default function Notice ({ icon, message, children }) {
  return (
    <div className='row'>
      <div
        className={`d-flex flex-column p-2 align-items-center 
                justify-content-center text-center col-sm-12
                ${style.box} ${style.orangecolor} ${style.whitecolor}`}
      >
        <h1 className='display-1'>
          { icon }
        </h1>

        <h2 className={`mb-5 p-2 ${style.textWhite}`}>{message}</h2>

        {children}
      </div>
    </div>
  )
}
