import React from 'react'
import Notice from '../Notice'

import { render } from '@testing-library/react'

const successIcon = <i className="far fa-check-circle"></i>
const failIcon = <i className="far fa-times-circle" ></i>

const successNotice = (
  <Notice message={'Notice success'} icon={successIcon} >
    Notice Children
  </Notice>
)

const failNotice = (
    <Notice message={'Notice success'} icon={failIcon}>
      Notice Children
    </Notice>
)

it("NOTICE - success notice", () => {
    const { container } = render(successNotice)

    const successIcon = container.querySelector(".fa-check-circle")

    expect(successIcon).toBeInTheDocument()
})

it("NOTICE - failure notice", () => {
    const { container } = render(failNotice)

    const failureIcon = container.querySelector(".fa-times-circle")

    expect(failureIcon).toBeInTheDocument()
})