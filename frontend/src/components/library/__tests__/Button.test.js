import React from 'react'
import Button from '../Button'

import { render } from '@testing-library/react'

const clickHandler = () => console.log("I'm the clickhandler")
const disabled = true
const classes = 'additional-class'

it('Library - Button', () => {
  const { debug, container } = render(
    <Button clickHandler={clickHandler} disabled={disabled} classes={classes}>
      ChildrenProps
    </Button>
  )

  const renderButton = container.querySelector(`.${classes}`)
  expect(renderButton).toBeInTheDocument()
})
