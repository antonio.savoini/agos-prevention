import React from 'react'

export default function Button ({ children,
  clickHandler = () => {},
  disabled, classes }) {
  return (
    <button
      className={`btn btn-color d-flex justify-content-center align-items-center white-color font-weight-bold  btn-lg btn-block ${classes}`}
      disabled={disabled}
      onClick={e => {
        clickHandler(e)
      }}
    >
      {children}
    </button>
  )
}
