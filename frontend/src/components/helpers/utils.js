import { parse, format } from "date-fns";
import Moment from "moment-timezone";
import accounting from "accounting";
import _ from "lodash";

import { appConfig } from "../../config";

import moment from "moment";

const dateToUtc = (dateTime) => {
  return parse(dateTime, "dd/MM/yyyy HH:mm:ss", new Date()).toISOString();
};
const formatDate = (date) => {
  return format(new Date(date), "yyyy-MM-dd HH:mm:ss");
};

const converDateToEuroZone = (date) => {
  const schedule_time = Moment(date);
  return schedule_time.tz("Europe/Paris").toString();
};

const formatToCurrency = (amount) => {
  return accounting.formatMoney(amount, "€", "2", " ", ",");
};

/*const formatNumber = strNum => {
  return (+strNum / 100).toFixed(2)
}*/

/*/adds remaning zeros at the of a number
since javascript removes extra zeros after the decimal point
*/
const formatNumber = (amount) => {
  if (typeof amount === "number") {
    if (Number.isInteger(amount)) {
      return `${amount}.00`;
    } else if (amount.toString().split(".")[1].length === 1) {
      return `${amount}0`;
    } else {
      return amount;
    }
  } else {
    return amount;
  }
};

const sumTotalAmount = (param) => {
  let totalAmount = 0;
  _.mapValues(param, (values) => {
    if (values !== null) {
      totalAmount += parseFloat(values.replace(",", "."));
    }
  });
  return totalAmount.toFixed(2);
};

const validateEmail = (param = "") => {
  const emailRegex =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  return param.match(emailRegex) ? true : false;
};

//converts timestamp into a date format
const formatTimestamp = (timestamp = "") => {
  //2021-06-07-08.32.11.971564 => 07/06/2021
  const timestampArray = timestamp.split("-");
  return [timestampArray[2], timestampArray[1], timestampArray[0]].join("/");
};

const redirectToLanding = (token) => {
  window.location.assign(
    `${appConfig.PATH}/${appConfig.PATH_PREFIX}-landing/${token}`
  );
};

const allowAccess = (successCallback = () => {}, failCallback = () => {}) => {
  const date = new Date();
  const hours = date.getHours();
  const minutes = date.getMinutes();

  if ((hours > 8 || (hours === 8 && minutes > 30)) && hours < 22) {
    successCallback();
  } else {
    failCallback();
  }
};

const getCurrentDateTime = () => moment().format("YYYY-MM-DD HH:mm:ss");

const getCurrentDateWithAdd = (days,unit) => {
  return moment().add(days, unit).format("YYYY-MM-DD HH:mm:ss");
} 


export {
  dateToUtc,
  formatDate,
  converDateToEuroZone,
  formatToCurrency,
  sumTotalAmount,
  formatNumber,
  validateEmail,
  formatTimestamp,
  redirectToLanding,
  allowAccess,
  getCurrentDateTime,
  getCurrentDateWithAdd
};
