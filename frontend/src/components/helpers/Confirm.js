import { confirmAlert } from 'react-confirm-alert'; // Import
const Confirm = (title, message, cb) => {
    confirmAlert({
        title: title,
        message: message,
        buttons: [
            {
                label: 'Yes',
                onClick: () => cb()
            },
            {
                label: 'No',
            }
        ]
    });
}



export default Confirm
