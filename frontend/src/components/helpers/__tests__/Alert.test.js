import { AlertManager } from "../Alert";
import React from "react"
import {render} from "@testing-library/react"

import JQUERY from "../test_files/jquery"

window.$ = JQUERY


it("Alert - AlertManager - 201", () => {
    const { getByText } = render(AlertManager(201))

    expect(getByText("Successo")).toBeInTheDocument()
})

it("Alert - AlertManager - 500", () => {
    const { getByText } = render(AlertManager(500))

    expect(getByText("Errore")).toBeInTheDocument()
})