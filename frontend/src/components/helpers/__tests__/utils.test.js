import { formatToCurrency, formatNumber, validateEmail, formatTimestamp } from "../utils"

it("UTILS - formatToCurrency", () => {
    expect(formatToCurrency(20.00)).toBe("€20,00")
})

it("UTILS - formatNumber", () => {
    expect(formatNumber(20.0)).toBe("20.00")
})

it("UTILS - validateEmail - correct email", () => {
    expect(validateEmail("email@example.com")).toBe(true)
})

it("UTILS - validateEmail - wrong email", () => {
    expect(validateEmail("email@example")).toBe(false)
})

it("UTILS - formatDate", () => {
    expect(formatTimestamp("2021-06-07-08.32.11.971564")).toBe("07/06/2021")
})