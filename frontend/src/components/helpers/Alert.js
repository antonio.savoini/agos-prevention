import cheers from 'cheers-alert';
import 'cheers-alert/src/cheers-alert.css'; //load style
import 'font-awesome/css/font-awesome.min.css'; //load font-awesome

const Alert = (type,title,message,icon) => {
   
 return cheers[type]({
            title: title,
            message: message,
            alert: 'slideleft',
            icon: icon,//'fa-check-circle',
            duration: 10,
            callback: () => { }
        })
}

export function AlertManager (code) {
    switch (code) {
      case 200:
      case 201:
        Alert('success', 'Successo', 'Dati inviati con successo')
        break
      case 500:
      case 504:
        Alert('error', 'Errore', "Errore durante l'invio dei dati")
        break
    }
}

export default Alert
