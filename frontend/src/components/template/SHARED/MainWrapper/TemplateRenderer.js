import React, { useEffect } from "react";
import InvalidLink from "../MasterPage/InvalidLink";

import {
  // Response,
  // AxervePay,
  // StripePay,
  PayWith,
  NexiPayment,
} from "../index";

import PaymentSelect from "../PaymentSelect/PaymentSelect";
import { appConfig } from "../../../../config";

const BodyComponents = {
  [`${appConfig.PATH_PREFIX}-paywith`]: (token) => <PayWith token={token} />,
  [`${appConfig.PATH_PREFIX}-select`]: (token) => (
    <PaymentSelect token={token} />
    // <h2>seelct</h2>
  ),
  // [`${appConfig.PATH_PREFIX}-axerve`]: (token) => (
  //     <AxervePay token={token} />
  // ),
  // [`${appConfig.PATH_PREFIX}-stripe`]: (token) => (
  //     <StripePay token={token} />
  // ),
  [`${appConfig.PATH_PREFIX}-pay`]: (token) => <NexiPayment token={token} />,
  // [`${appConfig.PATH_PREFIX}-landing`]: (token) => (
  //     <Landing token={token} />
  // ),
  // [`return`]: () => (
  //     <Return />
  // )
};

export default function TemplateRenderer({ path, token }) {

  return (
    <div className="container">
        {path &&
        (BodyComponents[path.split("/")[2]] ? (
          BodyComponents[path.split("/")[2]](token)
        ) : (
          <InvalidLink />
        )
      )}
    </div>
  );
}
