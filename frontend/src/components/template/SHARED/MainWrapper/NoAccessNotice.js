import React from "react"

export default function NoAccessNotice() {
  return (
    <div className="d-flex justify-content-center align-items-center">
      <p className="text-center w-75">
        Non è possibile effettuare un pagamento ora, il servizio è disponibile
        tutti i giorni dalle 8:30 alle 22:00
      </p>
    </div>
  );
}
