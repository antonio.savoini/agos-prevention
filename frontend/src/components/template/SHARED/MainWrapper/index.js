import React, { useState, useEffect } from "react";
import { allowAccess } from "../../../helpers/utils";
import ApiFactory from "../../../../endpoints";
import { isEmpty, includes } from "lodash";

import MasterPage from "../MasterPage/MasterPage";
import TemplateRenderer from "./TemplateRenderer";
import NoAccessNotice from "./NoAccessNotice";
import { Landing } from "../../OCC41";
import { Return, QRCodePage, AgosWebPay } from "../../OCC44";
import { Response } from "../index";
import OCCStorageAPI from "../../../../OCCStorageAPI";

import { appConfig } from "../../../../config";

export default function MainWrappers(props) {
  const path = props.match.path;
  const onResponsePage = includes(path, "response")
  const onAgosWebPayPage = includes(path, "agoswebpay")

  const urlParams = new URLSearchParams(window.location.search);

  const token = onResponsePage ? urlParams.get("token") : props.match.params.token

  const [accessAllowed, setAccessAllowed] = useState(false);
  const [showAccessNotice, setShowAccessNotice] = useState(false);
  const [tokenValid, setTokenValid] = useState(0);

  useEffect(() => {
    allowAccess(siteIsAccessible, siteNotAccessible);
  }, []);

  function siteNotAccessible() {
    setShowAccessNotice(true);
  }

  function siteIsAccessible() {
    console.log("site is accessible")
    setAccessAllowed(true);

    if(onAgosWebPayPage) {
      setTokenValid(true)
    }
    else if(OCCStorageAPI.getSingle("responseToken") !== token && token !== 'undefined') {
      onResponsePage ? setTokenValid(true) : getURL()
    }
    else if(OCCStorageAPI.getSingle("responseToken") === token) {
      includes(path, "return") || includes(path, appConfig.QRCodePage) ? getURL() : setTokenValid(false)
    }
    //this case means token is undefined text
    else {
      setTokenValid(undefined)
    }
  }

  async function getURL() {
    console.log("calling get url")
    const { data } = await ApiFactory.TinyURL.TINYURL({ data: { token } });

    if (!isEmpty(data)) {
      const { isValid } = data;
      setTokenValid(isValid);
    } else {
      setTokenValid(undefined);
    }
  }

  return (
    <div className="container">
      {accessAllowed ? (
        <MasterPage expire={tokenValid}>
          {includes(path, "landing") ? (
            <Landing token={token} path={path} />
          ) : includes(path, "return") ? (
            <Return />
          ) : onResponsePage ? (
            <Response />
          ) : includes(path, appConfig.QRCodePage) ? (
            <QRCodePage />
          ) : onAgosWebPayPage ? (
            <AgosWebPay />
          ) :
          (
              <TemplateRenderer path={path} token={token} />
          )}
        </MasterPage>
      ) : (
        <MasterPage expire={true}>
          {showAccessNotice && <NoAccessNotice />}
        </MasterPage>
      )}
    </div>
  );
}
