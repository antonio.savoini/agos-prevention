import React from 'react'

import SubmitButton from './SubmitButton'

export default function Container ({
  contactName,
  idPratica,
  children,
  receiptOptions,
  formattedAmount,
  handleSubmit,
  numericAmount
}) {
  return (
    <div className='row'>
      <div style={{ marginTop: '-10px' }} className='col-sm-12'>
        {/* HEADER */}
        <div className='green-color text-center p-1 m-0'>
          <h4 className='font-weight-light text-capitalize contact-name-header'>
            Gentile {contactName}
          </h4>
          <h4 className='font-weight-light'>In relazione alla pratica n°</h4>
        </div>
        <div className='green-color text-center p-1 m-0'>
          <h2 className='text-uppercase pratica-header'>{idPratica}</h2>
        </div>

        {/* SEMI-HEADER */}
        <div className='text-center mt-3'>
          <div className='green-color mt-4'>
            <h5 className=' mb-4 font-weight-light'>
              Ti segnaliamo la Tua situazione contabile
            </h5>
            {children}
          </div>
        </div>

        {/* RECEIPT OPTIONS */}
        {receiptOptions}

        <SubmitButton
          formattedAmount={formattedAmount}
          handleSubmit={handleSubmit}
          disabled={numericAmount === 0}
        />
      </div>
    </div>
  )
}
