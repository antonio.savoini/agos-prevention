import React from 'react'
import EmailInput from './EmailInput'

import styles from '../../../../theme/Style.module.css'

export default function ReceiptOption ({
  changeHandler,
  checked,
  emailState,
  emailChangeHandler
}) {
  return (
    <section className='mt-5'>
      <p className="text-center">
        Desideri ricevere la ricevuta?
        <br />
        Nel caso di importi superiori a 77,47€ le verranno
        <br />
        addebitati 2€ per imposta di bollo.
      </p>

      <div className='mb-4'>
        <div
          className={`form-check`} // float-left
          style={{ zIndex: '20' }}
        >
          <input
            className='form-check-input'
            type='radio'
            name='flexRadioDefault'
            id='flexRadioDefault1'
            checked={checked === true}
            onChange={() => changeHandler(true)}
          />

          <label className='form-check-label' htmlFor='flexRadioDefault1'>
            Si
          </label>
        </div>

        {checked && (
          <>
            {/* <br /> */}
            <div
              className={`input-group w-100 ${styles['email-input-container']}`}
            >
              <EmailInput
                emailState={emailState}
                emailChangeHandler={emailChangeHandler}
                placeholder='Scrivere qui il vostro indirizzo email dove inviare la ricevuta...'
              />
            </div>
            <br />
          </>
        )}

        <div className={`form-check `}>
          {/* ${checked ? 'float-left' : ''} */}
          <input
            className='form-check-input'
            type='radio'
            name='flexRadioDefault'
            id='flexRadioDefault2'
            checked={checked !== true}
            onChange={() => changeHandler(false)}
          />

          <label
            className='form-check-label'
            htmlFor='flexRadioDefault2'
          >
            No
          </label>
        </div>

      </div>
    </section>
  );
}
