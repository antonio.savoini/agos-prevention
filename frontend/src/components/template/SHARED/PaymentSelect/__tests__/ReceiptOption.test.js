import React from 'react'
import ReceiptOption from '../ReceiptOption'
import { render } from '@testing-library/react'

class DummyProps {
  checked = true
  emailState = 'example@email.com'

  changeHandler (newValue) {
    this.checked = newValue
  }

  emailChangeHandler (newValue) {
    this.emailState = newValue
  }
}

const props = new DummyProps()

const TrueReceipt = (
  <ReceiptOption
    checked={props.checked}
    changeHandler={props.changeHandler}
    emailState={props.emailState}
    emailChangeHandler={props.emailChangeHandler}
  />
)

props.changeHandler(false)

const FalseReceipt = (
    <ReceiptOption
      checked={props.checked}
      changeHandler={props.changeHandler}
      emailState={props.emailState}
      emailChangeHandler={props.emailChangeHandler}
    />
  )

it('Flow Shared - ReceiptOption - checked true', () => {
  const { container } = render(TrueReceipt)

  const emailInput = container.querySelector('.form-control')
  const yesInput = container.querySelector('#flexRadioDefault1')

  expect(emailInput).toBeInTheDocument()
  expect(yesInput).toBeInTheDocument()
})

it('Flow Shared - ReceiptOption - checked false', () => {
  const { container } = render(FalseReceipt)

  const emailInput = container.querySelector('.form-control')
  const noInput = container.querySelector('#flexRadioDefault2')

  expect(emailInput).toBeNull();
  expect(noInput).toBeInTheDocument()
})
