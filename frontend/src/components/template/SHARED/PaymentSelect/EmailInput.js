import React from 'react'

export default function EmailInput ({
  emailState,
  emailChangeHandler,
  placeholder
}) {
  return (
    <input
      type='email'
      className='form-control'
      placeholder={placeholder}
      onChange={e => emailChangeHandler(e.target.value)}
      value={emailState}
    />
  )
}
