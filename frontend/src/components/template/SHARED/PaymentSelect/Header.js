import React from 'react'

export default function Header ({ contactName, idPratica }) {
  return (
    <>
      <div className='green-color text-center p-1 m-0'>
        <h4 className='font-weight-light text-capitalize contact-name-header'>
          Gentile {contactName}
        </h4>
        <h4 className='font-weight-light'>In relazione alla pratica n°</h4>
      </div>
      <div className='green-color text-center p-1 m-0'>
        <h2 className='text-uppercase pratica-header'>{idPratica}</h2>
      </div>
    </>
  )
}
