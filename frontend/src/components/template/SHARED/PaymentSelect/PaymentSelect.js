import React, { useState, useEffect } from 'react'

import Container from './Container'
import ReceiptOption from './ReceiptOption'

import OCCStorageAPI from '../../../../OCCStorageAPI'

import { FlowOCC41 } from "../../OCC41"
import { FlowOCC44 } from '../../OCC44'

import { useHistory } from 'react-router-dom'

import { formatToCurrency, validateEmail } from '../../../helpers/utils'
import Alert from '../../../helpers/Alert'

import { appConfig } from '../../../../config'

function PaymentSelect ({ token }) {
  const history = useHistory()
  const {
    CodicePratica,
    ContactName,
    email,
    FlagRicevuta,
    flowType,
    persistFormattedAmount,
    persistTotalAmount
  } = OCCStorageAPI.getAll()

  const amountsList = OCCStorageAPI.getSingle('amountsList')
  const productType = CodicePratica[1] === "2" ? "single" : "multi"

  const [wantReceipt, setWantReceipt] = useState(FlagRicevuta === 'SI')
  const [emailState, setEmailState] = useState(email)
  const [formattedAmount, setFormattedAmount] = useState(persistFormattedAmount)
  const [totalAmount, setTotalAmount] = useState(persistTotalAmount)
  const [taxAdded, setTaxAdded] = useState(false)

  useEffect(() => {
    initialTaxEvaluation()
  }, [])

  useEffect(() => {
    wantReceipt
      ? OCCStorageAPI.setSingle('FlagRicevuta', 'SI')
      : OCCStorageAPI.setSingle('FlagRicevuta', 'NO')
  }, [wantReceipt])

  useEffect(() => {
    OCCStorageAPI.setSingle('email', emailState)
  }, [emailState])

  useEffect(() => {
    OCCStorageAPI.setSingle('ImportoPagamento', totalAmount)
    handleReceiptWanted(wantReceipt)
  }, [totalAmount])

  function initialTaxEvaluation () {
    let amount = parseFloat(persistTotalAmount)

    if (amount > 77.47 && wantReceipt) {
      amount += 2.0
      setTaxAdded(true)
    }

    setTotalAmount(amount)
    setFormattedAmount(formatToCurrency(amount))
  }

  function handleReceiptWanted (checkedState) {
    let amount = totalAmount
    if (checkedState) {
      if (totalAmount > 77.47 && !taxAdded) {
        amount += 2.0
        setTaxAdded(true)
      }
    } else {  
      if (taxAdded && totalAmount > 77.47) {
        amount -= 2.0
      }

      setTaxAdded(false)
    }

    setTotalAmount(amount)
    setFormattedAmount(formatToCurrency(amount))
  }

  function handleReceiptClick (checkedState) {
    setWantReceipt(checkedState)
    handleReceiptWanted(checkedState)
  }

  function handleSubmit () {
    if (validateEmail(emailState)) {
      pushHistory()
    } else {
      if (wantReceipt) {
        Alert(
          'error',
          'Errore di validazione',
          'Email non valida',
          'fa-exclamation'
        )
      } else {
        pushHistory()
      }
    }
  }

  function pushHistory () {
    history.push(
      `${appConfig.PATH}/${appConfig.PATH_PREFIX}-paywith/${token}`,
      {
        amount: parseFloat(totalAmount).toFixed(2),
        format: formattedAmount,
        wantReceipt: wantReceipt ? 'SI' : 'NO',
        comingFromPage: true
      }
    )
  }

  return (
    <Container
      contactName={ContactName}
      idPratica={CodicePratica}
      receiptOptions={
        <ReceiptOption
          changeHandler={handleReceiptClick}
          checked={wantReceipt}
          emailChangeHandler={setEmailState}
          emailState={emailState}
        />
      }
      formattedAmount={formattedAmount}
      handleSubmit={handleSubmit}
      numericAmount={totalAmount}
    >
      {flowType === 'OCC41' || productType === "single" ? (
        <FlowOCC41 formattedAmount={persistFormattedAmount} />
      ) : (
        <FlowOCC44
          setTotalAmount={total => setTotalAmount(total)}
          setFormattedAmount={formatted => setFormattedAmount(formatted)}
          wantReceipt={wantReceipt}
          setTaxAdded={setTaxAdded}
          totalAmount={totalAmount} 
          amountsList={amountsList}
          taxAdded={taxAdded}
        />
      )}
    </Container>
  )
}

export default PaymentSelect
