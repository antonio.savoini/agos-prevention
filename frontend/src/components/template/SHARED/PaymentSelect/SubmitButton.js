import React from 'react'

import Button from '../../../library/Button'

export default function SubmitButton ({
  handleSubmit,
  disabled,
  formattedAmount
}) {
  return (
    <Button
      clickHandler={handleSubmit}
      disabled={disabled}
    >
      Paga {formattedAmount}
    </Button>
  )
}
