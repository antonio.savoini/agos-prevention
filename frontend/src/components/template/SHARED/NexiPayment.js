import React, { useEffect } from 'react'
import { useLocation, /*Redirect*/ } from 'react-router-dom'

import ApiFactory from '../../../endpoints'
import {AlertManager} from '../../helpers/Alert'
import { formatNumber, redirectToLanding } from '../../helpers/utils'
import OCCStorageAPI from '../../../OCCStorageAPI'

import Button from '../../library/Button'
import Spinner from '../../library/Spinner'

export default function NexiPayment ({ token }) {

  const comingFromPage = useLocation()?.state?.comingFromPage || false
  
  useEffect(() => {
    !comingFromPage && redirectToLanding(token)
  }, [])

  const [disable, setDisable] = React.useState(false)

  const CodicePratica = OCCStorageAPI.getSingle('CodicePratica')
  const ImportoPagamento = OCCStorageAPI.getSingle('ImportoPagamento')

  async function handlePaymentProcedure () {
    try {
      setDisable(true)

      const { data } = await ApiFactory.Mercury.INIT({
        amount: formatNumber(ImportoPagamento),
        CodicePratica,
        token
      })
      setTimeout(()=> {
        window.location.assign(data.url)
      }, 3000)
    } catch (err) {

      AlertManager(500)
      console.log('\nError:\n', err)
    }
  }

  return (
    <>
      {comingFromPage ? (
        <div className='row d-flex justify-content-center'>
          <div className='col-sm-12'>
            {
              disable && <p className="font-weight-light text-center" >Al termine del pagamento attendere il completamento delle operazioni.</p>
            }
            <Button
              clickHandler={handlePaymentProcedure}
              disabled={disable}
              // id='pagaBtn'
            >
              {!disable ? (
                'Procedi al pagamento'
              ) : (
                <Spinner />
              )}
            </Button>
          </div>
        </div>
      ) : (
        <></>
        // <Redirect
        //   to={`${appConfig.PATH}/${appConfig.PATH_PREFIX}-landing/${token}`}
        // />
      )}
    </>
  )
}
