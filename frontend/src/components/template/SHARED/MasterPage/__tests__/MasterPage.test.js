import React from "react";

import MasterPage from "../MasterPage";
import { render } from "@testing-library/react"

const MasterPageExpire0 = <MasterPage expire={0} >MasterPage 1 childern</MasterPage>
const MasterPageExpireUndefined = <MasterPage expire={undefined} >MasterPage 2 childern</MasterPage>
const MasterPageExpireTrue = <MasterPage expire={true} >MasterPage 3 childern</MasterPage>
const MasterPageExpireFalse = <MasterPage expire={false} >MasterPage 4 childern</MasterPage>

it('Masterpage - expire 0', () => {
    const { getByText } = render(MasterPageExpire0)

    expect(getByText("Caricamento...")).toBeInTheDocument()
});

it('Masterpage - expire undefined', () => {
    const { getByText } = render(MasterPageExpireUndefined)

    expect(getByText("Il link non è valido")).toBeInTheDocument()
});

it('Masterpage - expire true', () => {
    const { getByText } = render(MasterPageExpireTrue)

    expect(getByText("MasterPage 3 childern")).toBeInTheDocument()
});

it('Masterpage - expire false', () => {
    const { getByText } = render(MasterPageExpireFalse)

    expect(getByText("Questo link è scaduto, ti preghiamo di contattarci.")).toBeInTheDocument()
});