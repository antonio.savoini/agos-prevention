import React from 'react'
import style from '../../../../theme/Style.module.css';

import Button from "../../../library/Button"

const Expire = () => {
    return (
        <div className="row">
            <div className="col-sm-12">
                <div className={`text-center ${style.greencolor}`}>
                    <h3 className="mb-5">Questo link è scaduto, ti preghiamo di contattarci.</h3>
                    <h1 className="text-center display-3">
                    <i className="fas fa-phone"></i>
                    </h1>
                </div>
            </div>
            {/* <div className="col-sm-12">
                <div className="text-center mt-3 p-5">
                    <div className="d-flex justify-content-center">
                        <a href="tel:02270701"
                            role="button">
                            <Button>
                                CHIAMACI
                            </Button>
                        </a>
                    </div>
                </div>
            </div> */}
        </div>
    )
}

export default Expire
