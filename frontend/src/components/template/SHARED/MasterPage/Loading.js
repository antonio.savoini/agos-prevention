import React from 'react'

export default function () {
  return (
    <div className={`container`}>
      <div className='row h-100 w-100 d-flex justify-content-center align-items-center'>
        <div id='feedback'>
          {/* <Loader type='TailSpin' width={90} height={90} color='#1aaeb8' /> */}
          <h3 className='text-center'>Caricamento...</h3>
        </div>
      </div>
    </div>
  )
}
