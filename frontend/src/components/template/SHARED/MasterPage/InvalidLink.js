import React from 'react'

export default function () {
  return (
    <div className='container h-100'>
      <div className='row h-100 justify-content-center align-items-center'>
        <div id='feedback'>
          <h3 className='text-center'>Il link non è valido</h3>
        </div>
      </div>
    </div>
  )
}
