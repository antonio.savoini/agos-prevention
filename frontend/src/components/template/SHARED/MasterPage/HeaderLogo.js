import React from "react"
import Logo from '../../img/logo.jpg'

export default function HeaderLogo() {
    return <img src={Logo} alt="logo" className="img-fluid w-25 mt-3" />
}