import React from 'react'
import Expire from './Expire'

import HeaderLogo from './HeaderLogo'
import Loading from './Loading'
import InvalidLink from './InvalidLink'

const MasterPage = ({ children, expire }) => {
  return (
    <>
      <div className='row'>
        <div className='col-sm-12 pb-5  d-flex justify-content-center'>
          <HeaderLogo />
        </div>
      </div>

      {
      expire === 0 ? (<Loading />) 
        : expire === undefined ? (<InvalidLink />)
        : expire ? (children)
        : (<Expire />)
      }
    </>
  )
}

export default MasterPage
