import React, {useEffect} from 'react'
import { useHistory, useLocation, Redirect } from 'react-router-dom'

import { appConfig } from '../../../config'
// import '../../../styles/style.css'

import Button from '../../library/Button'

import { redirectToLanding } from '../../helpers/utils'

import OCCStorageAPI from "../../../OCCStorageAPI"

const PayWith = props => {
  const history = useHistory()
  const location = useLocation()

  const comingFromPage = location.state?.comingFromPage
  const { token } = props

  useEffect(()=>{
    !comingFromPage && redirectToLanding(token)
  }, [])
 
  const amount = location.state?.amount
  const format = location.state?.format
  const wantReceipt = location.state?.wantReceipt
  

  const handleSubmit = control => {
    switch (control) {
      case 'pay':
        history.push(
          `${appConfig.PATH}/${appConfig.PATH_PREFIX}-pay/${token}`,
          {
            amount,
            format,
            wantReceipt,
            comingFromPage: true
          }
        )
        break
      default:
    }
  }

  return (
    <>
      {comingFromPage ? (
        <div className='row'>
          <div style={{ marginTop: '-10px' }} className='col-sm-12'>
            <div className='text-center green-color p-1 m-0'>
              <h5 className='font-weight-light'>
                Scegli il metodo di pagamento che preferisci
              </h5>
            </div>
            <div className='text-center mt-3'>
              <div className='mt-2'>
                <Button clickHandler={() => handleSubmit('pay')}>
                  <i className='fas fa-credit-card'></i> Carta di credito
                </Button>

                <Button clickHandler={() => history.goBack()}>
                  Torna Indietro
                </Button>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <></>
        // <Redirect
        //   to={`${appConfig.PATH}/${appConfig.PATH_PREFIX}-landing/${token}`}
        // />
      )}
    </>
  )
}

export default PayWith
