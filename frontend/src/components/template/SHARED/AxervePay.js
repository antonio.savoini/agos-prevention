import React, { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom';
import { appConfig } from '../../../config';
import createReactClass from 'create-react-class';
import Alert from '../../helpers/Alert';
// import '../css/style.css';
import "react-datepicker/dist/react-datepicker.css";
import _ from 'lodash'
import ApiFactory from '../../../endpoints'

const AxervePay = () => {
    
    const location = useLocation();
    const token = location.state.token;
    const amount = location.state.amount;
    const [paymentConfig, setPaymentConfig] = useState({});
    
    useEffect(() => {
        getPaymentToken();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

   

   const getPaymentToken = async () => {
        try {
            const company_id = appConfig.COMPANYID;
            const _amount = amount.substring(0, amount.length - 2) + "." + amount.substring(amount.length - 2)
            const { data } = ApiFactory.Pay.AXERVE({ company_id ,amount:_amount });
            
            if(!_.isEmpty(data.payload)){
                setPaymentConfig(data.payload);
            } else {
                Alert('error','Token Error',data.error.description);
                setPaymentConfig({error:data.error});
            }
        } catch (err) {
            Alert('error','Server Error', err);
        }
    }


    const ScriptRender = createReactClass({
        displayName: 'axerve',
        componentDidMount() {
            if (Object.keys(paymentConfig).length > 0 && !_.has(paymentConfig,'error')) {
                const script = document.getElementById('axerve').innerHTML;
                // eslint-disable-next-line no-eval
                window.eval(script);
            }
        },
        render() {
            return (<div dangerouslySetInnerHTML={{ __html: XpayScript }} />);
        }

    });

    window.axervePayment = async (result) => {
        
        try {
            ApiFactory.Pay.AXERVE_PAYMENT({result,token});
            result.status === 'OK' 
            ? document.location = `${appConfig.RESPONSEURL}?status=OK`
            : document.location = `${appConfig.RESPONSEURL}?status=KO&error=${result.error.description}&token=${token}`;
                            
        } catch (err) {
            console.log("window.axervePayment -> err", err)
            
        }
    }

    const XpayScript = `
    <script type='text/javascript' id="axerve">
        axerve.lightBox.shop = 'GESPAY80027';
        axerve.lightBox.open('${paymentConfig.paymentID}','${paymentConfig.paymentToken}',(res)=>{
           if(res !== undefined){
               const result = JSON.parse(res);
               console.log("result", result);
               
               if(result.hasOwnProperty('error')){
                   axervePayment(result);
               }
    }
        });
    </script>
   `;

    return (
        <div className="row d-flex justify-content-center">
            <div style={{ marginTop: "-10px" }} className="col-sm-12">
                <div className="text-center green-color p-3 m-0">
                    <h3>PAGAMENTO</h3>
                </div>
                {
                    _.has(paymentConfig,'error') && <h1 className="text-center">Payment cannot be processed.</h1>
                }
                <div id="modal"></div>
                <ScriptRender />
            </div>
        </div>




    )
}

export default AxervePay




