// export { default as TemplateRenderer } from "./TemplateRenderer/TemplateRenderer.js" 

export { default as AxervePay } from "./AxervePay"
export { default as NexiPayment } from "./NexiPayment"
export { default as PayWith } from "./PayWith"
export { default as Response } from "./Response"
export { default as StripePay } from "./StripePay"

export { default as MainWrapper } from "./MainWrapper"