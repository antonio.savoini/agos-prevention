import React, { useState } from "react";
import style from "../../../theme/Style.module.css";
import { appConfig } from "../../../config";

import Loading from "./MasterPage/Loading";

import ApiFactory from "../../../endpoints";

// import { AlertManager } from "../../helpers/Alert";

import { formatTimestamp, formatNumber } from "../../helpers/utils";

import OCCStorageAPI from "../../../OCCStorageAPI";

import Button from "../../library/Button";

import Notice from "../../library/Notice";

import Spinner from "../../library/Spinner";

const codeMatchObject = {
  0: "PGOK",
  "000": "PGOK",

  100: "PGKO",
  106: "PGKO",
  117: "PGKO",
  122: "PGKO",
  125: "PGKO",
  200: "PGKO",
  903: "PGKO",

  101: "PGKI",
  118: "PGKI",
  119: "PGKI",
  902: "PGKI",

  102: "PGKF",
  104: "PGKF",
  111: "PGKF",
  126: "PGKF",
  129: "PGKF",
  202: "PGKF",
  208: "PGKF",
  209: "PGKF",
  210: "PGKF",

  109: "PGKT",
  115: "PGKT",
  120: "PGKT",
  182: "PGKT",
  888: "PGKT",
  904: "PGKT",
  906: "PGKT",
  907: "PGKT",
  908: "PGKT",
  909: "PGKT",
  911: "PGKT",
  913: "PGKT",
  999: "PGKT",

  110: "PGKM",
  116: "PGKM",
  121: "PGKM",
  123: "PGKM",
  204: "PGKM",
};

/*
QR CODE INCASSO REQUEST OBJECT
{
   "CodiceIstitutoQr":"AG",
   "CodicePraticaQr":"01000000006545695401",
   "CodicePanQr":null,
   "CodiceAzioneEsitoQr":"OK",
   "CodCollettoreQr":"5123",
   "FormaPagQr":"POV",
   "ImportoPagamentoQr":"44.90",
   "CodAutorizQr":"392702",
   "TimestampPagaQr":"2021-06-05-22.31.01.000000",
   "DatiIncassoSetefiQr":"",
   "FlagRicevutaQr":"SI"
}
*/

const Response = () => {
  const urlParams = new URLSearchParams(window.location.search);
  const status = urlParams.get("status");
  const error = urlParams.get("error");
  const token = urlParams.get("token");
  const message = urlParams.get("message") || "";
  //const maskedpan = urlParams.get('maskedpan')
  const responsecode = urlParams.get("responsecode");
  const authorizationcode = urlParams.get("authorizationcode");
  const paymentid = urlParams.get("paymentid");
  const result = urlParams.get("result");
  const securitytoken = urlParams.get("securitytoken");
  const cardtype = urlParams.get("cardtype");
  const paymentID = urlParams.get("paymentid");
  const customfield = urlParams.get("customfield");

  const CodiceAzioneEsito = _determineCodiceAzioneEsito();
  const TimestampPaga = _generateTimeStampFormat();

  const {
    FlagRicevuta,
    CodicePratica,
    ProgrAzione,
    CodCollettore,
    CodiceIstituto,
    // TimestampPaga,
    email,
    CodicePan,
    flowType,
  } = OCCStorageAPI.getAll();

  const initialAmount = customfield > 77.47 && FlagRicevuta === "SI" ? customfield - 2 : customfield

  const [opsCompleted, setOpsCompleted] = useState(false);
 
  const [incassoPayCompleted, setIncassoPayCompleted] = useState({
    completed: false,
    status: "pending",
  });
  const [azioneToPayCompleted, setAzioneToPayCompleted] = useState({
    completed: false,
    status: "pending",
  });


  React.useEffect(() => {
    if (!OCCStorageAPI.getSingle("responseToken")) {
      if (status === "ok") {
        //calling if OK
         (flowType === "OCC41" || flowType === "OCC44") ? sendToIncassoPay() : sendToIncassoPayQR()
        if (FlagRicevuta === "SI") {
          console.log("sending receipt request....");
          sendReceipt({
            common: {
              timestamp: formatTimestamp(TimestampPaga),
              initialAmount: formatNumber(initialAmount)
                .toString()
                .replace(".", ","),
              totalAmount: formatNumber(customfield)
                .toString()
                .replace(".", ","),
              pratica: CodicePratica,
              email,
            },
            agos: {
              agosTransId: paymentid,
              agosOutcome: CodiceAzioneEsito === "PGOK" ? "OK" : "KO",
              OP: paymentID,
            },
            serviceProvider: {
              terminalID: securitytoken,
              type: cardtype,
              responseCode: result,
              refCode: "---",
              authCode: authorizationcode,
              serviceTransId: paymentid,
              serviceOutcome: CodiceAzioneEsito,
            },
          });
        }

        OCCStorageAPI.clear();
        OCCStorageAPI.setSingle("responseLink", window.location.href);
        OCCStorageAPI.setSingle("responseToken", token);
      } else {
        //calling if KO
        if(flowType === "OCC41" || flowType === "OCC44") {
          sendAzioneToPay()
        } else {
          setAzioneToPayCompleted({ completed: true, status: "success" })
        }
      }
    } else {
      setOpsCompleted(true)
    }
  }, []);

  React.useEffect(checkOpsState, [
    azioneToPayCompleted,
    incassoPayCompleted,
  ]);

  React.useEffect(() => {
    console.table({ CodiceAzioneEsito, opsCompleted, incassoPayCompleted, azioneToPayCompleted })
  }, [CodiceAzioneEsito, opsCompleted, incassoPayCompleted, azioneToPayCompleted])

  function _generateTimeStampFormat() {
    const date = new Date();
    const month = _determineFormat(date.getMonth() + 1);
    const day = _determineFormat(date.getDate());
    const hours = _determineFormat(date.getHours());
    const minutes = _determineFormat(date.getMinutes());
    const seconds = _determineFormat(date.getSeconds());

    const timestamp = `${date.getFullYear()}-${month}-${day}-${hours}.${minutes}.${seconds}.000000`;

    return timestamp;
  }

  function _determineFormat(number) {
    if (number < 10) {
      return `0${number}`;
    } else {
      return number;
    }
  }

  const handleBack = () => {
    window.location.assign(
      `${appConfig.PATH}/${appConfig.PATH_PREFIX}-landing/` + token
    );
  };

  function _determineCodiceAzioneEsito() {
    return responsecode && codeMatchObject[responsecode]
      ? codeMatchObject[responsecode]
      : codeMatchObject["100"];
  }

  async function sendToIncassoPayQR() {
    console.log("incasso pay qr function")
    const incassoPayQRObject = {
      CodiceIstitutoQr: CodiceIstituto,
      CodicePanQr:CodicePan,
      CodiceAzioneEsitoQr: CodiceAzioneEsito,
      
      FormaPagQr: "POV",
      DatiIncassoSetefiQr: "",

      CodAutorizQr: authorizationcode,
      CodicePraticaQr: CodicePratica,
      CodCollettoreQr: CodCollettore,
      ImportoPagamentoQr: customfield,
      TimestampPagaQr: TimestampPaga,
      FlagRicevutaQr: FlagRicevuta,
    }
    try {
      let apiResponse = await ApiFactory.Agos.POST_INCASSO_PAY_QR(
        incassoPayQRObject
      );
      console.log("incassopayqr response: ", apiResponse)
      setIncassoPayCompleted({ completed: true, status: "success" });
      // AlertManager(apiResponse.data.statusCode);
    } catch (err) {
      // AlertManager(err.statusCode);
      setIncassoPayCompleted({ completed: true, status: "fail" });
      console.log("Error: ", err);
    }

  }

  async function sendToIncassoPay() {
    console.log("incasso pay function")
    const incassoPayObject = {
      CodiceIstituto,
      CodicePan,
      CodiceAzioneEsito,
      FormaPag: "POV",
      DatiIncassoSetefi: "",
      CodAutoriz: authorizationcode,
      CodicePratica,
      ProgrAzione,
      CodCollettore,
      ImportoPagamento: customfield,
      TimestampPaga,
      FlagRicevuta,
    };

    try {
      let apiResponse = await ApiFactory.Agos.POST_INCASSO_PAY(
        incassoPayObject
      );

      console.table({ "incasso response": apiResponse.data.statusCode })

      setIncassoPayCompleted({ completed: true, status: "success" });

      // AlertManager(apiResponse.data.statusCode);
    } catch (err) {
      // AlertManager(err.statusCode);

      console.log("Error: ", err);
      setIncassoPayCompleted({ completed: true, status: "fail" });
    }
  }

  async function sendAzioneToPay() {
    try {
      let apiResponse = await ApiFactory.Agos.AZIONE_TO_PAY({
        CodiceIstituto,
        CodicePratica,
        CodicePan,
        ProgrAzione,
        CodCollettore,
        CodiceAzioneEsito,
      });

      console.log("response from azione to pay: ", apiResponse);
      setAzioneToPayCompleted({ completed: true, status: "success" });
      // AlertManager(apiResponse.data.statusCode);
    } catch (err) {
      console.log("error in azione pay: \n", err);
      setAzioneToPayCompleted({ completed: true, status: "fail" });
      // AlertManager(err.statusCode);
    }
  }

  async function sendReceipt(storageObject) {
    console.log("receipt request sent");
    try {
      let res = await ApiFactory.Agos.RECEIPT(storageObject);
      console.log("receipt api response: ", res);
    } catch (err) {
      console.log("Error in email sending from response: ", err);
    }
  }

  function checkOpsState() {
    if (status === "ok" && incassoPayCompleted.completed) {
      setOpsCompleted(true);
    } else if (status === "ko" && azioneToPayCompleted.completed) {
      setOpsCompleted(true);
    }
  }

  function showSuccess() {
    if (
      (status === "ok" && incassoPayCompleted.status === "success") ||
      (status === "ko" && azioneToPayCompleted.status === "success")
    ) {
      return true;
    } else if (
      (status === "ok" && incassoPayCompleted.status === "fail") ||
      (status === "ko" && azioneToPayCompleted.status === "fail")
    ) {
      return false;
    }
  }



  return (
    <>
      {token ? (
        <Notice
          icon={
            !opsCompleted ? (
              <Spinner />
            ) : (
              <i
                className={
                  showSuccess() && status === "ok"
                    ? "far fa-check-circle"
                    : "far fa-times-circle"
                }
              ></i>
            )
          }
          message={
            !opsCompleted
              ? "Prego attendere che l'operazione venga completata"
              : showSuccess() && status === "ok"
              ? "Il tuo pagamento è avvenuto correttamente."
              : "Sembra che ci sia stato un problema col pagamento."
          }
        >
          {status !== "ok" && (
            <>
              {opsCompleted && (
                <div className="d-flex justify-content-center mb-5">
                    <Button
                      clickHandler={handleBack}
                      classes={`${style.btngreen} ${style.whitecolor}`}
                    >
                      RIPROVA
                    </Button>
                </div>
              )}
            </>
          )}
        </Notice>
      ) : (
        <Loading />
      )}
    </>
  );
};

export default Response;
