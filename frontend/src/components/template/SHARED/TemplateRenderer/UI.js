import React from 'react'

import MasterPage from '../MasterPage/MasterPage'
import Landing from '../../OCC41/Landing.js'

import Return from "../../OCC44/Return" 

import {
  Response,
  AxervePay,
  StripePay,
  PayWith,
  NexiPayment,
} from '../index'

import { appConfig } from '../../../../config'
// import { QRCodePage } from '../../OCC44'

import PaymentSelect from "../PaymentSelect/PaymentSelect"

import InvalidLink from '../MasterPage/InvalidLink'
/**
 * QRCode page URL with queries: /agosui/wait?pratica='...'&azienda='AG'
 */

const PathComponents = {
  // [`${appConfig.QRCodePage}`]: () => (
  //     <QRCodePage />
  // ),
  [`${appConfig.PATH_PREFIX}-response`]: () => (
      <Response />
  ),
  [`${appConfig.PATH_PREFIX}-paywith`]: (token) => (
      <PayWith token={token} />
  ),
  [`${appConfig.PATH_PREFIX}-select`]: (token) => (
      <PaymentSelect token={token} />
  ),
  [`${appConfig.PATH_PREFIX}-axerve`]: (token) => (
      <AxervePay token={token} />
  ),
  [`${appConfig.PATH_PREFIX}-stripe`]: (token) => (
      <StripePay token={token} />
  ),
  [`${appConfig.PATH_PREFIX}-pay`]: (token) => (
      <NexiPayment token={token} />
  ),
  [`${appConfig.PATH_PREFIX}-landing`]: (token) => (
      <Landing token={token} />
  ),
  [`return`]: () => (
      <Return />
  )
} 

export default function TemplateRendererUI ({ path, expire, token }) {
  return (
    <div className='container'>
      {path &&
        (PathComponents[path.split('/')[2]] ? (
          <MasterPage expire={expire}>
            {PathComponents[path.split('/')[2]](token)}
          </MasterPage>
        ) : (
          <InvalidLink />
        ))}
    </div>
  )
}
