import React from 'react'

import TemplateRendererUI from '../UI'

import { appConfig } from '../../../../../config'
import { render } from '@testing-library/react'

import { createMemoryHistory } from 'history'

import { Router } from 'react-router'

const props = {
  path: `${appConfig.PATH}/${appConfig.PATH_PREFIX}-landing/token`,
  expire: true,
  token: 'token'
}

const returnPageProps = { ...props, path: `${appConfig.PATH}/return` }

const history = createMemoryHistory()

it('TemplateRendererUI - Landing', () => {
  const { getByTestId } = render(
    <Router history={history}>
      <TemplateRendererUI path={props.path} expire={props.expire} token={props.token} />
    </Router>
  )

  expect(getByTestId("landing-component")).toBeInTheDocument()
})

it("TemplateRendererUI - Return", () => {
    const { getByTestId } = render(
        <Router history={history}>
            <TemplateRendererUI path={returnPageProps.path}
            expire={returnPageProps.expire} token={returnPageProps.token} />
        </Router>
    )

    expect(getByTestId("return-component")).toBeInTheDocument()
})