import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { appConfig } from "../../../config";

import OCCStorageAPI from "../../../OCCStorageAPI";

import Button from "../../library/Button";

import ApiFactory from "../../../endpoints";

import Alert from "../../helpers/Alert";

import { formatToCurrency, getCurrentDateWithAdd } from "../../helpers/utils";

import Loading from "../SHARED/MasterPage/Loading";

const Landing = ({ token, path }) => {
  const history = useHistory();
  const [buttonDisabled, setButtonDisabled] = useState(true);
  const [flowType, setFlowType] = useState("");

  useEffect(() => {
    getContactDetail();
  }, []);

  useEffect(() => {
    if (flowType === "OCC41" && buttonDisabled) {
      setTimeout(() => {
        setButtonDisabled(false);
      }, 600);
    }
  }, [flowType]);

  async function getContactDetail() {
    try {
      const {
        data: { request_id },
      } = await ApiFactory.TinyURL.GET_CONTACT({ data: { token } });
      getRequestDetail(request_id);
    } catch (err) {
      Alert("error", "Error Occured !!", err.message, "fas fa-exclamation");
    }
  }

  async function getRequestDetail(requestID) {
    const { data } = await ApiFactory.Request.GET_REQUEST_DETAILS(requestID);

    if (data[0]) {
      data[0].data49 === "OCC41" ? assignOCC41Data(data[0]) :
      _flowOCC44QRManager(data[0], requestID)
    }
  }

  function _flowOCC44QRManager(dataObject, requestID) {
    assignOCC44Data(dataObject, requestID);

    dataObject.data48 === "true" ? handleSubmit() : getEncodeString(dataObject)
}

  async function getEncodeString(paramsObject) {
    const codiceFiscale = paramsObject.data18;
    const url = "https://agosdv.cloudando.com/agosui/return?x=1";
    const limite = getCurrentDateWithAdd(2,'days');

    const { data } = await ApiFactory.Agos.GET_ENCODE_STRING({
      codiceFiscale,
      vendorID: appConfig.VENDOR_ID,
      url,
      limite,
    });

    window.location.assign(appConfig.BASEURL(data.encodedString));
    // window.location.assign(
    //   `${appConfig.PATH}/return?token=${data.encodedString}`
    // );
  }

  function assignOCC44Data(dataObject, requestID) {
    const {
      idpratica,
      data7,
      data9,
      data4,
      data49,
      data1,
      created_at,
      data23,
      contact_name,
    } = dataObject;

    setFlowType("OCC44");
    const productType = idpratica[1] === "2" ? "single" : "multi";
    const amountsListData = _formatData(dataObject);
    let totalAmount = 0;
    amountsListData.map((value) => (totalAmount += +value["IMP-RATA"]));

    amountsListData.sort((a, b) => +a["NR-RATA"] - +b["NR-RATA"]);

    _assignOCCStorage([
      idpratica,
      data9,
      null,
      created_at,
      data1 || "                ",
      parseFloat(productType === "single" ? data23 : totalAmount), //importo pagamento
      "SI",
      data4,
      "5083",
      data7,
      contact_name,
      true,
      data49,
      formatToCurrency(productType === "single" && data49 === "OCC44" ? data23 : totalAmount), //persiste formatted amount
      parseFloat(productType === "single" && data49 === "OCC44" ? data23 : totalAmount), // persist total amount
      amountsListData,
      requestID,
      token,
      
    ]);
  }

  function assignOCC41Data(dataObject) {
    setFlowType("OCC41");

    let {
      importo,
      contact_name,
      idpratica,
      data7,
      data9,
      data4,
      data6,
      data1,
    } = dataObject;

    _assignOCCStorage([
      idpratica,
      data9,
      OCCStorageAPI.doesExist() && OCCStorageAPI.getSingle("TimestampPaga"),
      data6,
      data1 || "                ",
      +importo,
      "SI",
      data4,
      "5083",
      data7,
      contact_name,
      true,
      "OCC41",
      formatToCurrency(+importo),
      +importo,
      [],
      undefined,
      undefined,
      "OCC41",
    ]);
  }

  function _formatData(rawObject) {
    //amount keys
    const amountKeys = ["data3", "data6", "data21", "data12"];
    //date keys
    const dateKeys = ["data20", "data2", "data10", "data13"];
    //rata num keys
    const rataKeys = ["data5", "data8", "data11", "data14"];

    const finalData = [];

    for (let i = 0; i < amountKeys.length; i++) {
      if (rawObject[amountKeys[i]] && +rawObject[amountKeys[i]] > 0 ) {
        finalData.push({
          CHECKED: true,
          "IMP-RATA": parseFloat(rawObject[amountKeys[i]]),
          "DT-RATA": rawObject[dateKeys[i]],
          "NR-RATA": rawObject[rataKeys[i]],
          FORMAT: formatToCurrency(rawObject[amountKeys[i]]),
        });
      }
    }

    return finalData;
  }

  function _assignOCCStorage([
    CodicePratica,
    CodiceIstituto,
    TimestampPaga,
    TimestampPagaOCC41,
    CodicePan,
    ImportoPagamento,
    FlagRicevuta,
    ProgrAzione,
    CodCollettore,
    email,
    ContactName,
    comingFromStorage,
    flowType,
    persistFormattedAmount,
    persistTotalAmount,
    amountsList,
    requestID,
    usertoken,
]) {
    OCCStorageAPI.setAll({
      CodicePratica,
      CodiceIstituto,
      TimestampPaga,
      TimestampPagaOCC41,
      CodicePan,
      ImportoPagamento,
      FlagRicevuta,
      ProgrAzione,
      CodCollettore,
      email,
      ContactName,
      comingFromStorage,
      flowType,
      persistFormattedAmount,
      persistTotalAmount,
      amountsList,
      requestID,
      usertoken,
    });
  }

  const handleSubmit = () => {
    history.push(`${appConfig.PATH}/${appConfig.PATH_PREFIX}-select/${token}`, {
      comingFromPage: true,
    });
  };

  return (
    <>
      {flowType === "OCC41" ? (
        <div className="row" data-testid="landing-component">
          <div style={{ marginTop: "-10px" }} className="col-sm-12">
            <div className="p-1 m-0">
              <h3>Agos</h3>
              <p className="font-weight-light">Completa ora il pagamento</p>
            </div>
            <Button clickHandler={handleSubmit} disabled={buttonDisabled}>
              Paga ora{" "}
              {!buttonDisabled &&
                OCCStorageAPI.getSingle("persistFormattedAmount")}
            </Button>
          </div>
        </div>
      ) : flowType === "OCC44" ? (
        <h1 className="text-center mt-5">Verifica dei dati in corso...</h1>
      ) : (
        <Loading />
      )}
    </>
  );
};

export default Landing;
