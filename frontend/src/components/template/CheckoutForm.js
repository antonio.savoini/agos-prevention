import React, { useEffect, useState } from 'react';
import useForm from 'react-hook-form';
import { useStripe, useElements, CardElement } from '@stripe/react-stripe-js';
import CardSection from './CardSection';
import Cards from './img/cards.png';
import { useLocation } from 'react-router-dom';
import { appConfig } from '../../config';
// import '../../styles/style.css';
import ApiFactory from '../../endpoints'
export default function CheckoutForm() {

  const location = useLocation();
  const token = location.state.token;
  const amount = location.state.amount;
  const [secretKey, setSecretKey] = useState();
  const [formValues, setFormValues] = useState({});
  const { register, errors, setValue, handleSubmit } = useForm();



  useEffect(() => {
    initiatePayment();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const stripe = useStripe();
  const elements = useElements();

  const initiatePayment = async () => {
    const company_id = appConfig.COMPANYID;
    const { data } = ApiFactory.Pay.STRIPE({ company_id ,amount });
    setSecretKey(data);
  }


  const handleControl = (formControl, value) => {
    setFormValues({ ...formValues, [formControl]: value });
    setValue(formControl, value);
  }

  const handleFormSubmit = async (e) => {

    if (!stripe || !elements) {
      // Stripe.js has not yet loaded.
      // Make sure to disable form submission until Stripe.js has loaded.
      return;
    }


    const result = await stripe.confirmCardPayment(secretKey, {
      payment_method: {
        card: elements.getElement(CardElement),
        billing_details: {
          name: formValues['contact_name'],
        },
      }
    });

    if (result.error) {

      const errorObj = result.error;

      delete errorObj['payment_intent'];
      delete errorObj['payment_method'];

      errorObj.token = token;
      errorObj.amount = amount;
      errorObj.status = 'KO';

      ApiFactory.Pay.STRIPE_PAYMENT(errorObj);
      document.location = `${appConfig.RESPONSEURL}?status=KO&error='${result.error.message}'&token=${token}`;

    } else {
      // The payment has been processed!
      if (result.paymentIntent.status === 'succeeded') {
        const {
          amount,
          capture_method,
          client_secret,
          confirmation_method,
          currency,
          status,
          created
        } = result.paymentIntent;

        ApiFactory.Pay.STRIPE_PAYMENT({
          amount,
          capture_method,
          client_secret,
          confirmation_method,
          currency,
          status,
          created,
          token
        });

        document.location = `${appConfig.RESPONSEURL}?status=OK`;
                           
      }
    }
  };

  return (
    <div className="row d-flex justify-content-center">
      <div style={{ marginTop: "-10px" }} className="col-sm-12">
        <form onSubmit={handleSubmit(handleFormSubmit)}>
          <div className="text-center green-color p-3 m-0">
            <h3>PAGAMENTO</h3>
          </div>
          <div>
            <label className="control-label col-form-label col-form-label-lg">Intestatario carta di credito</label>
            <img src={Cards} width="90" alt="cards" className=" ml-2 img-fluid" />
            <input
              className="form-control text-capitalize form-control-lg"
              name="contact_name"
              onChange={e => handleControl('contact_name', e.target.value)}
              type="text"
              value={formValues['contact_name'] || ''}
              ref={register({ name: 'contact_name' }, { required: { value: true, message: 'Field is required.' } })}
            />
            <p className="error-text">{errors.contact_name && errors.contact_name.message}</p>

          </div>
          <div id="labeler">
            <label>Numbero Carta</label>
            <label className="float-right mr-5">CVV</label>
            <label className="float-right">Scadenza</label>

          </div>
          <CardSection />
          <div className="d-flex justify-content-center">
            <button className="btn btn-color white-color font-weight-bold btn-lg" disabled={!stripe}>Stripe Pay <i className="fab fa-stripe h3"></i></button>
          </div>
        </form>
      </div>
    </div>
  );
}