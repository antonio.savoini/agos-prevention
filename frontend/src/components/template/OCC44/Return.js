
import React, { useEffect, useState } from "react";

import { useLocation } from "react-router";

import ApiFactory from "../../../endpoints";

import OCCStorageAPI from "../../../OCCStorageAPI";

import Notice from "../../library/Notice";

/**
 * Test encoded string: 0FFBCFBC92176B778950A1F4C2
 */

import Expire from "../SHARED/MasterPage/Expire"

export default function Return() {
  const encodedString = new URLSearchParams(useLocation().search).get("token");
  const [authSuccess, setAuthSuccess] = useState(false);
  const [showUI, setShowUI] = useState(false);
  const [changeMessage, setChangeMessage] = useState(false);

  useEffect(() => {
    console.log("encoded string: ", encodedString);
    //no responseToken  in LS, so it means first time
    if(!OCCStorageAPI.getSingle("responseToken")) {
      getDecodeString();
    }
  }, []);

  async function getDecodeString() {
    const {data} = await ApiFactory.Agos.GET_DECODE_STRING({
      encodedString
    });
    console.log('statusCode + result',data );
    if (data.statusCode === 200 && data.result === 'OK') {
      setAuthSuccess(true);
      const token = OCCStorageAPI.getSingle("usertoken");
      const requestID = OCCStorageAPI.getSingle("requestID")
      // OCCStorageAPI.setSingle("visited", true);

      UpdateRequest(requestID)

      setTimeout(() => {
        setChangeMessage(true);
      }, 1000)

      setTimeout(() => {
        window.location.assign(`/agosui/agosui-select/${token}`)
      }, 3000);
    } else {
      setAuthSuccess(false);
    }

    setShowUI(true);
  }

  async function UpdateRequest(requestID) {
    try {
      let response = await ApiFactory.Agos.UPDATE_REQUEST({ request_id:requestID, data48: 'true' })

      console.log(`response from update request: `, response)
    } catch(err) {
      console.log(`update request failed`)
      console.log(err)
    }
  }

  return (
    <div className="text-center container" data-testid="return-component">
      {
        OCCStorageAPI.getSingle("responseToken") ? <Expire />
        : showUI ? (
          authSuccess ? (
            <>
              {!changeMessage ? (
                <Notice
                  icon={<i className={"far fa-check-circle"}></i>}
                  message="Autenticazione avvenuta con successo"
                />
              ) : (
                <h2>Reindirizzamento in corso...</h2>
              )}
            </>
          ) : (
            <Notice
              icon={<i className={"far fa-times-circle"}></i>}
              message="Autenticazione avvenuta con successo"
            />
          )
        ) : (
          <></>
        )
      }
    </div>
  );
}
