import React from "react";
import { formatToCurrency } from "../../helpers/utils";

import { mapValues } from "lodash";

import Checklist from "./AmountChecklist";

import Alert from "../../helpers/Alert"

export default function FlowOCC44({
  setTotalAmount,
  setFormattedAmount,
  wantReceipt,
  amountsList,
  taxAdded,
}) {
  /**
   * TWO PARALLEL ARRAYS SYSTEM:
   * checklistState array keeps the labels' descriptions
   * checkedArray keeps the checkbox checked state(true or false)
   *
   * These arrays are synchronized by their index
   */
  const [checklistState, setChecklistState] = React.useState([]);
  const [checkedArray, setCheckedArray] = React.useState([]);
  const [showChecklist, setShowChecklist] = React.useState(false);

  React.useEffect(createCompatibleChecklistData, []);

  React.useEffect(() => {
    if (checklistState.length > 0) {
      if (!showChecklist) {
        setShowChecklist(true);
      }
    } else {
      setShowChecklist(false);
    }
  }, [checklistState]);

  /*CREATES AN APPROPRIATE DATA STRUCTURE
    FOR THE CHECKOBOX AMOUNTS LIST */
  function createCompatibleChecklistData() {
    /* TOTAL AMOUNT */
    let valuesSum = 0;

    /* THIS PARALLEL ARRAY KEEPS THE BOOLEAN CHECKED STATES OF CHECKBOX LIST */
    const checklistChecked = [];

    mapValues(amountsList, (value) => {
      if (value !== null) {
        valuesSum += value["IMP-RATA"];
        checklistChecked.push(true);
      }
    });

    setTotalAmount(valuesSum);
    setFormattedAmount(formatToCurrency(valuesSum));

    setChecklistState(amountsList);
    setCheckedArray(checklistChecked);
  }

  function handleChecklistClickManager(index) {
    if (index === 0) {
      if (
        !checkedArray[index + 1] &&
        !checkedArray[index + 2] &&
        !checkedArray[index + 3]
      ) {
        handleChecklistClick(index);
      }
      
      else {
        Alert("info", "Attenzione", "La selezione è consentita soltanto in ordine")
      }
    }
    
    else if (index === 1) {
      if (!checkedArray[index + 1] && !checkedArray[index + 2] && checkedArray[index-1]) {
        handleChecklistClick(index);
      } else {
        Alert("info", "Attenzione", "La selezione è consentita soltanto in ordine")
      }
    }
    
    else if (index === 2) {
      if (!checkedArray[index + 1] && checkedArray[index-1]) {
        handleChecklistClick(index);
      }
      
      else {
        Alert("info", "Attenzione", "La selezione è consentita soltanto in ordine")
      }
    }
    
    else if (index === 3) {
      if (!checkedArray[index + 1] && checkedArray[index - 1]) {
        handleChecklistClick(index);
      }
      
      else {
        Alert("info", "Attenzione", "La selezione è consentita soltanto in ordine")
      }
    }
  }

  function handleChecklistClick(index) {
    const updated = checklistState.map((amount, index2) =>
      index === index2 ? (amount.CHECKED = !amount.CHECKED) : amount.CHECKED
    );

    let filteredAmount = 0;

    updated.map((item, index) => {
      if (item) {
        filteredAmount += checklistState[index]["IMP-RATA"];
      }
    });

    filteredAmount = parseFloat(filteredAmount.toFixed(2));

    if (wantReceipt && filteredAmount > 77.47 && taxAdded) {
      filteredAmount += 2.0;
    }

    setCheckedArray(updated);
    setTotalAmount(filteredAmount);
    setFormattedAmount(formatToCurrency(filteredAmount));
  }

  return (
    <div className="text-center w-100">
      {/* flow occ44 */}
      {showChecklist ? (
        checklistState.map((singleAmount, index) => (
          <div className="form-check" key={index}>
            <Checklist
              amount={singleAmount["IMP-RATA"]}
              checked={checkedArray[index]}
              expiry={singleAmount["DT-RATA"]}
              rata={singleAmount["NR-RATA"]}
              value={singleAmount["FORMAT"]}
              key={index}
              index={index}
              changeHandler={handleChecklistClickManager}
            />
          </div>
        ))
      ) : (
        <h4>Caricamento dei dati in corso</h4>
      )}
    </div>
  );
}
