import React from 'react'
import { render } from '@testing-library/react'

import FlowOCC44 from '../FlowOCC44'

import OCC44MockResponse from '../../../../OCC44MockResponse.json'

const props = {
  setTotalAmount: function () {},
  setFormattedAmount: function () {},
  wantReceipt: true,
  amountsListEmpty: [],
  amountsList: OCC44MockResponse.RATA
}

it('FLowOCC44 - empty', () => {
  const { container } = render(
    <FlowOCC44
      setFormattedAmount={props.setFormattedAmount}
      setTotalAmount={props.setTotalAmount}
      wantReceipt={props.wantReceipt}
      amountsList={props.amountsListEmpty}
    />
  )

  expect(container.children[0].firstChild).toBeNull()
})

it('FLowOCC44 - full', () => {
    const { container } = render(
      <FlowOCC44
        setFormattedAmount={props.setFormattedAmount}
        setTotalAmount={props.setTotalAmount}
        wantReceipt={props.wantReceipt}
        amountsList={props.amountsList}
      />
    )
  
    expect(container.children[0].firstChild).toBeInTheDocument()
  })
  