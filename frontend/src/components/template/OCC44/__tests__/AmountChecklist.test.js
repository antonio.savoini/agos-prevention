import React from 'react'
import { render } from '@testing-library/react'

import AmountChecklist from "../AmountChecklist"

import { formatToCurrency } from "../../../helpers/utils"

const props = {
    value: formatToCurrency(20.22),
    rata: 1,
    expiry: "07/07/2007",
    index: 0,
    checked: true,
    changeHandler: function (index, event) {
        console.log(`Index: ${index} - Event: ${event}`)
        this.checked = !this.checked
    },
    amount: 20.22
}

it("AmountChecklist true", ()=> {
    const CheckBox = <AmountChecklist {...props} />

    const { getByTestId } = render(CheckBox)

    const AmountCheckbox = getByTestId("amount-checkbox")
    expect(AmountCheckbox.checked).toEqual(true)
})

it("AmountChecklist false", ()=> {
    props.changeHandler(0, 'event') //setting checked to false
    const CheckBox = <AmountChecklist {...props} />

    const { getByTestId } = render(CheckBox)

    const AmountCheckbox = getByTestId("amount-checkbox")
    expect(AmountCheckbox.checked).toEqual(false)
})