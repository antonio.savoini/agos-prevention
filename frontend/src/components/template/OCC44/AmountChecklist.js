import React from 'react'

const labelStyles = {
  width: 'auto',
  padding: '0 6px'
}

const Checklist = ({ value, rata, expiry, index, checked, changeHandler, amount }) => (
  <>
    <input
      data-testid="amount-checkbox"
      type='checkbox'
      className='form-check-input'
      onChange={() => changeHandler(index)}
      checked={checked}
      name={amount}
      id={`select${index}`}
    />

    <label className='form-check-label text-center' htmlFor={`select${index}`}
    onClick={() => changeHandler(index)}
    style={{ userSelect: "none" }}
    >
      <label style={labelStyles}>{value} </label>
      <label style={labelStyles}> - {`Rata n°${rata} - `} </label>
      <label style={labelStyles}>{expiry}</label>
    </label>
  </>
)

export default Checklist