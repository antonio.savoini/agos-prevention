import React, { useState } from "react";

/*
Form checks:
id pratica:
* only numbers
* max length: 16
* user inserts only the 8 numbers rest i fill with zeros
tipo === finanziamento ? first two chars '01' : '02'

*/

import Alert from "../../../helpers/Alert";

import Button from "../../../library/Button";

import ApiFactory from "../../../../endpoints";

import { formatDate } from "../../../helpers/utils"

export default function AgosWebPay() {
  const [inputState, setInputState] = useState("");
  const [typeState, setTypeState] = useState("finanziamento");

  function validatePraticaInput() {
    if (/^\d+$/.test(inputState)) {
      if(inputState.length >= 8 && inputState.length <= 16) {
        const finalPratica = _formatPratica();
        webRequest(finalPratica)
      } else {
        Alert(
          "error",
          "Errore di validazione",
          "Il codice pratica deve avere almeno 8 caratteri"
        );
      }
    } else {
      Alert(
        "error",
        "Errore di validazione",
        "Controllare che il codice pratica sia correttamente inserito"
      );
    }
  }


  async function webRequest(pratica) {
    const forwardURL = `https://agosdv.cloudando.com/agosui/qrcode?pratica=${pratica}&azienda=AG`

    const paramsObject = {
      timestamp: formatDate(new Date()),
      product: typeState === "finanziamento" ? "1" : "2",
      id_pratica: pratica,
      codice_istituto: "AG",
      url: forwardURL
    }

    try {
      let response = await ApiFactory.Agos.WEB_REQUEST(paramsObject)

      if(response.data.statusCode === 201) {
        window.location.assign(
          // `http://localhost:3000/agosui/qrcode?pratica=${pratica}&azienda=AG`
          forwardURL
        );
      }
    } catch {
      Alert("error", "Errore", "Errore durante l'invio dei dati. Riprovare.")
    }
  }

  function _formatPratica() {
    let final = "";
    const diffLength = 16 - inputState.length;
    let zeros = "";
    
    for (let i = 0; i < diffLength; i++) {
      zeros += "0";
    }
    final = `${zeros}${inputState}01`;

    if (typeState === "finanziamento") {
      final = `01${final}`;
    } else {
      final = `02${final}`;
    }

    return final;
  }

  return (
    <div>
      <p className="text-center">
        Gentile utente, la preghiamo di inserire il codice
        <br />
        pratica e l'azienda di riferimento
      </p>

      <p className="text-center" >
      Inserire il codice pratica di 9 cifre. Per prodotto revolving<br/>
      ( carta di credito, finstock)
      
      il codice pratica è riportato sull’estratto conto.
      <br/>
      <strong>ATTENZIONE</strong> non inserire il codice Pan da 16 cifre.
      </p>

      <div className="d-flex justify-content-center pb-5">
        <form className="w-75 webpay-form" onSubmit={(e) => e.preventDefault()}>
          <div className="form-group">
            <label htmlFor="tipoSelect" className="font-weight-bold text-dark">
              Tipo:
            </label>
            <br />
            <select
              id="tipoSelect"
              className="form-control"
              onChange={(e) => {
                setTypeState(e.target.value);
              }}
            >
              <option value="finanziamento">Finanziamento</option>
              <option value="credit">Carta di credito</option>
            </select>
          </div>

          <div className="form-group">
            <label
              htmlFor="praticaInput"
              className="font-weight-bold text-dark"
            >Pratica:
            </label>
            <br />
            <input
              maxLength="16"
              minLength="9"
              type="text"
              id="praticaInput"
              name="praticaInput"
              value={inputState}
              onChange={(event) => setInputState(event.target.value)}
              className="form-control"
              // pattern="\d{0,9}"
            />
          </div>

          <div className="form-group">
            <label
              htmlFor="aziendaSelect"
              className="font-weight-bold text-dark"
            >
              Azienda:
            </label>
            <br />
            <select id="aziendaSelect" className="form-control">
              <option>Agos</option>
            </select>
          </div>

          <Button clickHandler={validatePraticaInput}>Invia</Button>
        </form>
      </div>
    </div>
  );
}
