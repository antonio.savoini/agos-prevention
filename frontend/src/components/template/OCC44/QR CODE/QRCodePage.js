import React, { useEffect } from 'react'

import { appConfig } from '../../../../config.js'
import ApiFactory from '../../../../endpoints.js'

/*
Page path: /agosui/qrcode?pratica=...&azienda=...
*/

export default function QRCodePage() {
  const urlParams = new URLSearchParams(window.location.search)
  
  const CodicePratica = urlParams.get('pratica')
  const CodiceIstituto = urlParams.get('azienda')

  useEffect(() => {
    doClickToQR()
  }, [doClickToQR])

  async function doClickToQR () {
    if (CodicePratica && CodiceIstituto) {
      try {
        console.log("in the try")
            const { data: { message } } = await ApiFactory.Agos.DO_CLICK_TO_QR({
            CodiceIstituto,
            CodicePratica
          })
    
        setTimeout(() => {
          window.location.assign(message)
        }, 1000)
      } catch(e) {
        console.log("Error in do click to qr: ", e)
      }
    }
  }

  return (
    <div>
      {/* <h1 className='text-center mt-5'>Verifica dei dati in corso...</h1> */}
      <h1 className='text-center mt-5'>Reindirizzamento in corso...</h1>
    </div>
  )
}
