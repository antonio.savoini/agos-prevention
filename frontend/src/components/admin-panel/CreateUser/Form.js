import React from 'react'

export default function Form ({
  handleFormSubmit,
  handleSubmit,
  handleControl,
  errors,
  handleBlur,
  formValues,
  register,
  userExist,
  watch
}) {
  return (
    <form onSubmit={handleSubmit(handleFormSubmit)}>
      <div className='form-group'>
        <label className='mr-3'>Role Type</label>

        <select
          name='role'
          className='form-control  form-control-lg'
          onChange={e => handleControl('role', e.target.value)}
          ref={register(
            { name: 'role' },
            { required: { value: true, message: 'Field is required.' } }
          )}
        >
          <option value=''>Select type</option>
          <option value='2'>Team Lead</option>
          <option value='3'>Agent</option>
        </select>

        <p className='error-text'>{errors.role && errors.role.message}</p>

        <label className='mr-3'>Username</label>
        <input
          className='form-control  form-control-lg'
          name='username'
          onChange={e => handleControl('username', e.target.value)}
          onBlur={handleBlur}
          type='text'
          value={formValues['username'] || ''}
          ref={register(
            { name: 'username' },
            {
              required: { value: true, message: 'Field is required.' },
              validate: {
                value: () => {
                  return !userExist.current ? true : 'Username already exists.'
                }
              }
            }
          )}
        />
        <p className='error-text'>
          {errors.username && errors.username.message}
        </p>
        <label className='mr-3'>Password</label>
        <input
          className='form-control  form-control-lg'
          autoComplete='new-password'
          name='password'
          onChange={e => handleControl('password', e.target.value)}
          type='password'
          value={formValues['password'] || ''}
          ref={register(
            { name: 'password' },
            {
              required: { value: true, message: 'Field is required.' },
              minLength: { value: 8, message: 'minimum length must be 8' }
            }
          )}
        />
        <p className='error-text'>
          {errors.password && errors.password.message}
        </p>
        <label className='mr-3'>Re-Password</label>
        <input
          className='form-control  form-control-lg'
          name='repassword'
          onChange={e => handleControl('repassword', e.target.value)}
          type='password'
          value={formValues['repassword'] || ''}
          ref={register(
            { name: 'repassword' },
            {
              validate: {
                value: value => {
                  return value === watch('password')
                    ? true
                    : 'Password not matched.'
                }
              },
              required: { value: true, message: 'Field is required.' },
              minLength: { value: 8, message: 'minimum length must be 8' }
            }
          )}
        />
        <p className='error-text'>
          {errors.repassword && errors.repassword.message}
        </p>
      </div>
      <button className='btn btn-primary float-right'>Submit</button>
    </form>
  )
}
