import React, { useState, useRef } from 'react'
import useForm from 'react-hook-form'

import Alert from '../../helpers/Alert'
import { appConfig } from '../../../config'
import ApiFactory from '../../../endpoints'
import Form from './Form'

export default function CreateUser () {
  const userExist = useRef()
  const [formValues, setFormValues] = useState({})
  const { register, errors, setValue, watch, handleSubmit } = useForm()

  const handleControl = (formControl, value) => {
    setFormValues({ ...formValues, [formControl]: value })
    setValue(formControl, value)
  }

  const handleBlur = async () => {
    try {
      if (formValues.username !== ('' || undefined)) {
        const token = localStorage.getItem('token')
        const { data } = ApiFactory.Users.USER_EXISTS(formValues['username'], {
          headers: { authorization: token }
        })
        userExist.current = data
      }
    } catch (err) {
      Alert(
        'success',
        'Server Error',
        `Couldn't check users.`,
        'fa-check-circle'
      )
    }
  }

  const handleFormSubmit = async () => {
    try {
      const token = localStorage.getItem('token')
      formValues.company_id = appConfig.COMPANYID
      const res = await ApiFactory.Users.CREATE_USER(formValues, {
        headers: { authorization: token }
      })
      if (res === 'token_expired') {
        localStorage.setItem('token', 'token_expired')
        Alert('error', 'Session expired', 'Session expired.', 'fa-exclamation')
      } else {
        Alert(
          'success',
          'Operation Successfull',
          `User successfull created.`,
          'fa-check-circle'
        )
      }
    } catch (err) {
      Alert(
        'error',
        'Error Occured !!',
        err.response.data.message,
        'fas fa-exclamation'
      )
    }
  }

  return (
    <div className='col-lg-8'>
      <h1>Create User</h1>
      <hr />
      <Form
        handleFormSubmit={handleFormSubmit}
        handleSubmit={handleSubmit}
        handleControl={handleControl}
        errors={errors}
        handleBlur={handleBlur}
        formValues={formValues}
        register={register}
        userExist={userExist}
        watch={watch}
      />
    </div>
  )
}
