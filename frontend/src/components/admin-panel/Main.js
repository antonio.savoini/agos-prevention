import React from 'react'

import Header from './Header/Header'

const Main = ({ children }) => {
    return (
        <>
            <Header />
            <main role="main" className="container d-block">
                <div className="row">
                    <div className="container h-100 mt-1">
                        <div className="row h-100 justify-content-center align-items-center vertical-center">
                            {children}
                        </div>
                    </div>
                </div>
            </main>
        </>
    )
}

export default Main
