import React, { useState, useEffect } from 'react'

import Logo from '../../template/img/logo.jpg'

import { appConfig } from '../../../config'
import { decrypt } from '../../helpers/Encrypter'

import RoleTwo from './RoleTwo'

const Header = () => {
  const [role, setRole] = useState()
  const chunk = document.location.pathname.replace(/\//, '')

  useEffect(() => {
    const role = localStorage.getItem('role')
    if (role !== null) {
      setRole(decrypt(role))
    }
  }, [])

  const logout = () => {
    localStorage.removeItem('token')
    localStorage.removeItem('role')
    localStorage.removeItem('access_id')
  }

  return (
    <nav className='navbar navbar-expand-lg navbar-light bg-light shadow static-top'>
      <div className='container'>

        <a
          className='navbar-brand col-sm-2'
          href={`${appConfig.PATH}/dashboard`}
        >
          <img src={Logo} alt='logo' className='img-fluid' />
        </a>

        <button
          className='navbar-toggler'
          type='button'
          data-toggle='collapse'
          data-target='#navbarResponsive'
          aria-controls='navbarResponsive'
          aria-expanded='false'
          aria-label='Toggle navigation'
        >
          <span className='navbar-toggler-icon'></span>
        </button>

        <div className='collapse navbar-collapse' id='navbarResponsive'>

          <ul className='navbar-nav ml-auto'>
            
            { role === '2' ? <RoleTwo chunk={chunk} /> : null }
            
            <li className='nav-item'>
              <div className='dropdown'>
                <button
                  className='btn dropdown-toggle'
                  type='button'
                  id='dropdownMenuButton'
                  data-toggle='dropdown'
                  aria-haspopup='true'
                  aria-expanded='false'
                >
                  <svg
                    className='bi bi-gear-fill'
                    width='1em'
                    height='1em'
                    viewBox='0 0 16 16'
                    fill='currentColor'
                    xmlns='http://www.w3.org/2000/svg'
                  >
                    <path
                      fillRule='evenodd'
                      d='M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 01-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 01.872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 012.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 012.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 01.872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 01-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 01-2.105-.872l-.1-.34zM8 10.93a2.929 2.929 0 100-5.86 2.929 2.929 0 000 5.858z'
                      clipRule='evenodd'
                    />
                  </svg>
                </button>
                <div
                  className='dropdown-menu'
                  aria-labelledby='dropdownMenuButton'
                >
                  <a
                    className='dropdown-item'
                    href={`${appConfig.PATH}/login`}
                    onClick={() => logout()}
                  >
                    Logout
                  </a>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Header
