import React from 'react'

import {appConfig} from "../../../config"

export default function RoleTwo ({ chunk }) {
  return (
    <>
      <li className={`nav-item ${chunk === 'dashboard' ? 'active' : ''}`}>
        <a className='nav-link' href={`${appConfig.PATH}/dashboard`}>
          Home
          <span className='sr-only'>(current)</span>
        </a>
      </li>

      <li className={`nav-item ${chunk === 'create-user' ? 'active' : ''}`}>
        <a className='nav-link' href={`${appConfig.PATH}/create-user`}>
          Create User
        </a>
      </li>
      <li className={`nav-item ${chunk === 'payment-report' ? 'active' : ''}`}>
        <a className='nav-link' href={`${appConfig.PATH}/payment-report`}>
          {' '}
          <i className='fas fa-ad'></i> Pay Report
        </a>
      </li>
      <li className={`nav-item ${chunk === 'sms-report' ? 'active' : ''}`}>
        <a className='nav-link' href={`${appConfig.PATH}/sms-report`}>
          {' '}
          <i className='fas fa-ad'></i> SMS Report
        </a>
      </li>
    </>
  )
}
