import React from 'react'

const NotFound = () => {
    return (
        <div className="error-box pt-5">
            <div className="error-body text-center">
                <h1 className="error-title text-danger">404</h1>
                <h3 className="text-uppercase error-subtitle">PAGINA NON TROVATA !</h3>
                <p className="text-muted m-t-30 m-b-30">
                    SEMBRA CHE STAI CERCANDO DI TROVARE LA TUA STRADA DI CASA
                </p>
                <a href="/"
                className="btn btn-danger btn-rounded waves-effect waves-light mb-4 text-white">
                    Ritorna alla homepage
                </a>
            </div>
        </div>
    )
}

export default NotFound