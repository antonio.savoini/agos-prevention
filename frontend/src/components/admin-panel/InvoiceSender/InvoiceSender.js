import React, { useState, useEffect } from 'react'
import useForm from 'react-hook-form'

import { isArray } from 'lodash'

import Alert from '../../helpers/Alert'
import ApiFactory from '../../../endpoints'
import Form from './Form'

const InvoiceSender = () => {
  const [formValues, setFormValues] = useState({})
  const [companyOptions, setCompanyOptions] = useState([])
  const { register, errors, setValue, handleSubmit } = useForm()

  useEffect(() => {
    companyList()
  }, [])

  const handleControl = (formControl, value) => {
    setFormValues({ ...formValues, [formControl]: value })
    setValue(formControl, value)
  }

  const companyList = async () => {
    try {
      const token = localStorage.getItem('token')
      const { data } = await ApiFactory.Company.COMPANY_LIST({
        headers: { authorization: token }
      })
      const options = []
      if (isArray(data)) {
        data.map(({ customer_code, company_name }) =>
          options.push({ label: company_name, value: customer_code })
        )
        setCompanyOptions(options)
      } else if (data === 'token_expired') {
        localStorage.setItem('token', 'token_expired')
      }
    } catch (err) {
      Alert(
        'error',
        'Error Occured !!',
        'Error occured while inserting record.',
        'fas fa-exclamation'
      )
      console.log(err)
    }
  }

  const handleFormSubmit = async () => {
    try {
      const token = localStorage.getItem('token')
      //removing extra characters from amount
      formValues.amount = formValues.amount.replace(/\D+/g, '')

      const res = ApiFactory.NWG.SEND_INVOICE(formValues, {
        headers: { authorization: token }
      })

      if (res === 'token_expired') {
        localStorage.setItem('token', 'token_expired')
        Alert('error', 'Session expired', 'Session expired.', 'fa-exclamation')
      } else {
        Alert('success', 'Operation Successfull', res.data, 'fa-check-circle')
      }
    } catch (err) {
      console.log('handleFormSubmit -> err', err)
      Alert(
        'error',
        'Error Occured !!',
        err.response.data.message,
        'fas fa-exclamation'
      )
    }
  }

  return (
    <div className='col-lg-8'>
      <h1>
        <svg
          className='bi bi-cursor-fill'
          width='1em'
          height='1em'
          viewBox='0 0 16 16'
          fill='currentColor'
          xmlns='http://www.w3.org/2000/svg'
        >
          <path
            fillRule='evenodd'
            d='M14.082 2.182a.5.5 0 01.103.557L8.528 15.467a.5.5 0 01-.917-.007L5.57 10.694.803 8.652a.5.5 0 01-.006-.916l12.728-5.657a.5.5 0 01.556.103z'
            clipRule='evenodd'
          />
        </svg>{' '}
        Invoice Send
      </h1>
      <hr />

      <Form
        handleSubmit={handleSubmit}
        handleFormSubmit={handleFormSubmit}
        handleControl={handleControl}
        register={register}
        errors={errors}
        companyOptions={companyOptions}
        formValues={formValues}
      />
    </div>
  )
}

export default InvoiceSender
