import React from 'react'

import { isEmpty } from 'lodash'
import CurrencyInput from 'react-currency-input'

export default function Form ({
  handleSubmit,
  handleFormSubmit,
  handleControl,
  register,
  errors,
  companyOptions,
  formValues
}) {
  return (
    <form onSubmit={handleSubmit(handleFormSubmit)}>
      <label className='mr-3'>Customer Name</label>

      <input
        name='customer_name'
        className='form-control'
        onChange={e => handleControl('customer_name', e.target.value)}
        ref={register(
          { name: 'customer_name' },
          { required: { value: true, message: 'Field is required.' } }
        )}
      />

      <p className='error-text'>
        {errors.customer_name && errors.customer_name.message}
      </p>

      <div className='form-group'>
        <label className='mr-3'>Customer Code</label>

        <input
          className='form-control'
          name='customer_code'
          onChange={e => handleControl('customer_code', e.target.value)}
          value={formValues['customer_code']}
          disabled={isEmpty(companyOptions)}
          ref={register(
            { name: 'customer_code' },
            { required: { value: true, message: 'Field is required.' } }
          )}
        />

        <p className='error-text'>
          {errors.customer_code && errors.customer_code.message}
        </p>
      </div>

      <label className='mr-3'>Mobile Number</label>
      <div className='input-group'>
        <div className='input-group-prepend'>
          <span className='input-group-text'>+39</span>
        </div>
        <input
          className='form-control'
          name='mobile_number'
          onChange={e => handleControl('mobile_number', e.target.value)}
          type='tel'
          maxLength='11'
          value={formValues['mobile_number'] || ''}
          ref={register(
            { name: 'mobile_number' },
            {
              required: { value: true, message: 'Field is required.' },
              minLength: { value: 8, message: 'Minimum length must be 10' },
              pattern: { value: /[\d]{10}/, message: 'Only numbers allowed' }
            }
          )}
        />
      </div>

      <p className='error-text'>
        {errors.mobile_number && errors.mobile_number.message}
      </p>

      <label className='mr-3'>Amount</label>

      <CurrencyInput
        className='form-control'
        decimalSeparator=','
        thousandSeparator='.'
        precision='2'
        prefix='€'
        maxLength='11'
        name='amount'
        value={formValues['amount'] || ''}
        onChange={value => handleControl('amount', value)}
        ref={register(
          { name: 'amount' },
          {
            required: { value: true, message: 'Field is required.' }
          }
        )}
      />

      <p className='error-text'>{errors.amount && errors.amount.message}</p>

      <label className='mr-3'>Invoice File</label>

      <input
        className='form-control'
        name='invoice'
        onChange={e => handleControl('invoice', e.target.value)}
        type='text'
        value={formValues['invoice'] || ''}
        ref={register(
          { name: 'invoice' },
          {
            required: { value: true, message: 'Field is required.' },
            minLength: { value: 2, message: 'minimum length must be 2' }
          }
        )}
      />

      <p className='error-text'>{errors.invoice && errors.invoice.message}</p>

      <label className='mr-3'>Note</label>

      <textarea
        className='form-control'
        name='note'
        onChange={e => handleControl('note', e.target.value)}
        maxLength='255'
        value={formValues['note'] || ''}
        ref={register(
          { name: 'note' },
          {
            required: { value: true, message: 'Field is required.' }
          }
        )}
      />

      <p className='error-text'>{errors.note && errors.note.message}</p>

      <button className='btn btn-primary float-right'>Submit</button>
    </form>
  )
}
