import React from 'react'

export default function Form({ handleSubmit, handleFormSubmit, handleControl,
formValues, errors, handleLoginClick, register }) {

    return (
        <form onSubmit={handleSubmit(handleFormSubmit)}>
            <div className="form-group">
                <label className="mr-3">Username</label>
                <input className="form-control form-control-lg"
                    name="username" onChange={e => handleControl('username', e.target.value)}
                    type="text" value={formValues['username'] || ''}
                    ref={register({ name: 'username' },
                    {
                        required: { value: true, message: 'Field is required.' },
                    }
                )}/>
                <p className="error-text">
                    {errors.username && errors.username.message}
                </p>
                <label className="mr-3">Password</label>
                <input className="form-control form-control-lg"
                    autoComplete="new-password"
                    name="password"
                    onChange={e => handleControl('password', e.target.value)}
                    type="password" value={formValues['password'] || ''}
                    ref={register({ name: 'password' },
                    {
                        required: { value: true, message: 'Field is required.' },
                        minLength: { value: 8, message: 'minimum length must be 8' }
                    }
                )}/>
                <p className="error-text">
                    {errors.password && errors.password.message}
                </p>
            </div>
            <button className="btn btn-primary float-right"
            onClick={handleLoginClick} >Login</button>
        </form>
    )
}

