import React, { useState, useEffect } from 'react'
import useForm from 'react-hook-form'

import { isEmpty } from 'lodash'

import Alert from '../../helpers/Alert'
import { appConfig } from '../../../config'
import ApiFactory from '../../../endpoints'
import { encrypt } from '../../helpers/Encrypter'
import Form from './Form'

export default function Login (props) {

  const [formValues, setFormValues] = useState({})
  const { register, errors, setValue, handleSubmit } = useForm()

  useEffect(() => {
    checkTokenStatus()
    isAlreadyAuthenticated()
  }, [])

  function checkTokenStatus () {
    if (
      localStorage.getItem('token') === 'token_expired' &&
      isEmpty(formValues)
    ) {
      Alert(
        'error',
        'Sessione scaduta',
        'Sessione Scaduto per favore accedere di nuovo.',
        ''
      )
    }
  }

  function isAlreadyAuthenticated () {
    props.authenticated && props.history.push(`${appConfig.PATH}/dashboard`)
  }

  function handleControl (formControl, value) {
    setFormValues({ ...formValues, [formControl]: value })
    setValue(formControl, value)
  }

  async function handleFormSubmit (data) {
    try {
      data['company_id'] = appConfig.COMPANYID
      const result = await ApiFactory.Users.USER_VERIFY({ data })

      if (result.data.token) {
        localStorage.setItem('token', result.data.token)
        localStorage.setItem('role', encrypt(result.data.user_role_id))
        if (result.data.user_role_id == 0) {
          localStorage.setItem('access_id', encrypt(result.data.company_id))
        }
        window.location.reload()
      }
    } catch (err) {
      Alert('error', 'Error Occured !!', err.Error, 'fas fa-exclamation')
    }
  }

  function handleLoginClick () {
    handleSubmit(handleFormSubmit)
  }

  return (
    <div className='fluid-container h-100'>
      <div className='row h-100 justify-content-center align-items-center vertical-center'>
        <div className='col-md-6'>
          <div className='card'>
            <div className='form-horizontal'>
              <div className='card-body'>
                <h4 className='card-title'>Login</h4>

                <Form
                  formValues={formValues}
                  errors={errors}
                  handleControl={handleControl}
                  handleFormSubmit={handleFormSubmit}
                  handleSubmit={handleSubmit}
                  handleLoginClick={handleLoginClick}
                  register={register}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
