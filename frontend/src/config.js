const environment = process.env.REACT_APP_ENVIRONMENT;
const appConfig = {};
// const devServerURL = 'http://demo.sinerpay.com/';
const devServerURL = (token) => `https://customerid-c.agosducato.it/id/verifica.xhtml?token=${token}`
const prodServerURL = 'https://sinerpay.com/';
const customerServerURL = 'https://customer.sinerpay.com/';
const uatServerURL = 'https://uat.sinerpay.com/';

const baseURL = environment === 'prod' ? prodServerURL 
                : environment === 'customer' ? customerServerURL
                : environment === 'uat' ? uatServerURL
                : devServerURL;
                
if(environment === 'dev'){
    appConfig.BASEURL = baseURL;
    appConfig.COMPANYID = 125;
    appConfig.PATH = '/agosui';
    appConfig.PATH_PREFIX = 'agosui';
    appConfig.RESPONSEURL = `${baseURL}agosui/agosui-response`;
    appConfig.PAYPALAPI = '/api/pay/paypal';
    appConfig.REQUESTPAGE = 'agos';
    appConfig.XPAYSERVER = 'XPay.Environments.INTEG'
    appConfig.QRCodePage = 'qrcode'
    appConfig.VENDOR_ID = "agsREEXVBM54336hg"
}

export { appConfig };
