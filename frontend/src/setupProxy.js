const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
    app.use(createProxyMiddleware("/api", { target: "http://localhost:6002" }));
    //app.use(createProxyMiddleware("/agos/api", { target: "http://localhost:6003" }));
    app.use(createProxyMiddleware("/agos/agos-api", { target: "http://localhost:6002" }));

};
